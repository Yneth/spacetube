#ifdef GL_ES
precision highp float;
#endif

uniform float time;
uniform vec2 resolution;

const float pi = 3.1415926;

const vec3 ZERO_VECTOR = vec3(0., 0., 0.);
const vec3 COLOR_1 = vec3(0.1, 0.1, 0.1);//0.1, 0.5, 0.5
const vec3 COLOR_2 = vec3(0.1, 0.1, 0.2);//0.1, 0.1, 0.2
const vec3 COLOR_3 = vec3(0.8, 0.2, 1.0);//0.8, 0.2, 1.0
const vec3 COLOR_4 = vec3(0.1, 0.2, 0.2);//0.1, 0.2, 0.2

vec2 R = resolution;
vec2 Offset;
vec2 Scale=vec2(0.002,0.002);
float Saturation = 0.1; // 0 - 1;


vec3 lungth(vec2 x,vec3 c){
       return vec3(length(x+c.r),length(x+c.g),length(c.b));
}

void main( void ) {

	vec2 position = (gl_FragCoord.xy - resolution * 0.5) / resolution.yy * 0.6;
	float th = atan(position.y, position.x) / (1.0 * pi);
	float dd = length(position) + 0.005;
    float d = 0.5 / dd + time;

    vec2 x = gl_FragCoord.xy;

	vec3 uv = vec3(th + d, th - d, th + sin(d) * 0.45);

	float a = 0.5 + cos(uv.x * pi * 2.0) * 0.5;
	float b = 0.5 + cos(uv.y * pi * 2.0) * 0.5;
	float c = 0.5 + cos(uv.z * pi * 6.0) * 0.5;

	vec3 color = mix(COLOR_1,     COLOR_2,  pow(a, 0.2)) * 3.;
	color +=     mix(COLOR_3, 	  COLOR_2,  pow(b, 0.1)) * 0.75;
	color +=     mix(ZERO_VECTOR, COLOR_4,  pow(c, 0.1)) * 0.75;

	gl_FragColor = vec4( (color * dd), 1.0);
}