package main.ua.indigital.spacetube.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ua.abond.spacetube.SpaceTube;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Space Tube";
        config.width = 408;
        config.height = 408;
		new LwjglApplication(new SpaceTube(), config);
	}
}
