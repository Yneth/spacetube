package ua.abond.engine.physics;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.Disposable;
import ua.abond.engine.gameobject.components.RigidBody;

public interface GamePhysics<T extends RigidBody> extends Disposable {
    void update(float delta);
    void addRigidBody(T rigidBody3D);
    void removeRigidBody(T rigidBody3D);
    void drawDebug(Camera camera);
}
