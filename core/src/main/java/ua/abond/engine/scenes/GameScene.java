package ua.abond.engine.scenes;

import com.badlogic.gdx.utils.Disposable;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.gameobject.components.RigidBody;

// TODO: write javadoc
public interface GameScene extends Disposable {
    void onStart();

    GameObject instantiate(GameObject gameObject);
    void remove(GameObject gameObject);

    void update(final float deltaTime);

    Iterable<GameObject> getGameObjects();
    Iterable<Renderable> getRenderables();
    Iterable<RigidBody> getRigidBodies();

    Camera getMainCamera();
    void setMainCamera(Camera camera);
}
