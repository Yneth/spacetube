package ua.abond.engine;

import com.badlogic.gdx.utils.Disposable;

// TODO: Should be moved to engine folder
public interface GameController extends Disposable {
    void update(final float deltaTime);
    void render(final float deltaTime);
}
