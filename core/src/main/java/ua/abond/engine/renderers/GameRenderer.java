package ua.abond.engine.renderers;

import com.badlogic.gdx.utils.Disposable;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.physics.GamePhysics;
import ua.abond.engine.scenes.GameScene;

public interface GameRenderer extends Disposable {
    void render(final float deltaTime, final Iterable<GameObject> gameObjects);
    void render(final float deltaTime, GameScene scene);
    void renderDebug(final float deltaTime, final GamePhysics physics);
}
