package ua.abond.engine.gameobject.components;

public interface Component {
    void update(float deltaTime);
}
