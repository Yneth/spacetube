package ua.abond.engine.gameobject.components;

import ua.abond.engine.gameobject.components.Component;

public interface ComponentAdapter<Adaptee> extends Component {
    Adaptee getBody();
}
