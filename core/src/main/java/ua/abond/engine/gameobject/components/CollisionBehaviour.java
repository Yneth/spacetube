package ua.abond.engine.gameobject.components;

import ua.abond.engine.gameobject.GameObject;

public abstract class CollisionBehaviour implements Component {
    public final GameObject gameObject;

    public CollisionBehaviour(GameObject gameObject) {
        super();
        this.gameObject = gameObject;
    }

    @Override
    public void update(float deltaTime) {

    }

    public abstract void onContactStarted(GameObject collidedWith);

    public abstract void onContactProcessed(GameObject collidedWith);

    public abstract void onContactEnded(GameObject collidedWith);
}
