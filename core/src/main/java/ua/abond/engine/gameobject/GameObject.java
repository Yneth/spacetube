package ua.abond.engine.gameobject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Timer;
import ua.abond.engine.gameobject.components.Component;
import ua.abond.engine.gameobject.behaviours.Behaviour;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public class GameObject implements Cloneable {
    public static OnGameObjectCreatedListener onCreated;
    public static OnGameObjectDestroyedListener onDestroyedListener;

    private int uniqueIdentifier;

    public int layer;
    public String tag;
    public String name;
    public boolean active;

    // TODO: Change to ClassToInstanceArrayListMultimap
    private List<Component> components;

    public GameObject() {
        active = true;
        components = new ArrayList<Component>();
        uniqueIdentifier = IdGenerator.getNextId();
    }

    // TODO: Add component visitor class and visitable interface to eliminate instanceof
    public void update(final float deltaTime) {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            if (component instanceof Behaviour) {
                if (((Behaviour) component).activated) {
                    component.update(deltaTime);
                }
            } else {
                component.update(deltaTime);
            }
        }
    }

    public void addComponent(Component component) {
        components.add(component);
    }

    public <Type extends Component> Type getComponent(Class<Type> typeClass) {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            if (component.getClass().equals(typeClass) || component.getClass().getSuperclass().equals(typeClass)) {
                return (Type) component;
            }
        }
        return null;
    }

    public <Type extends Component> Type getComponentByInterface(Class<Type> typeClass) {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            List<Class> interfaces = getAllInterfaces(component.getClass());
            for (int j = 0; j < interfaces.size(); j++) {
                if (interfaces.get(j).equals(typeClass)) {
                    return (Type) component;
                }
            }
        }
        return null;
    }

    public <Type extends Component> List<Type> getComponents(Class<Type> typeClass) {
        return null;
    }

    public static GameObject instantiate(GameObject gameObject) {
        GameObject clone = null;
        try {
            clone = (GameObject) gameObject.clone();
        } catch (CloneNotSupportedException e) {
            Gdx.app.log(GameObject.class.getSimpleName(), "Failed to clone game object", e);
        }
        if (onCreated != null) {
            onCreated.onCreated(clone);
        }
        return clone;
    }

    public static void destroy(final GameObject gameObject) {
        gameObject.active = false;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (onDestroyedListener != null) {
                    onDestroyedListener.onDestroyed(gameObject);
                }
                gameObject.dispose();
            }
        }, 2.0f);
    }

    private void dispose() {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            if (component instanceof Disposable) {
                ((Disposable) component).dispose();
            }
        }
        components.clear();
    }

    private List<Class> getAllSuperClasses(Class cls) {
        if (cls == null) {
            return null;
        }

        List<Class> superclassesFound = new ArrayList();
        getAllSuperClasses(cls, superclassesFound);

        return superclassesFound;
    }

    private void getAllSuperClasses(Class cls, List<Class> superclassesFound) {
        while (cls != null) {
            superclassesFound.add(cls);
            cls = cls.getSuperclass();
        }
    }

    private List getAllInterfaces(Class cls) {
        if (cls == null) {
            return null;
        }

        List<Class> interfacesFound = new ArrayList();
        getAllInterfaces(cls, interfacesFound);

        return interfacesFound;
    }

    private void getAllInterfaces(Class cls, List<Class> interfacesFound) {
        while (cls != null) {
            Class[] interfaces = cls.getInterfaces();

            for (int i = 0; i < interfaces.length; i++) {
                if (!interfacesFound.contains(interfaces[i])) {
                    interfacesFound.add(interfaces[i]);
                    getAllInterfaces(interfaces[i], interfacesFound);
                }
            }

            cls = cls.getSuperclass();
        }
    }

    public int getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public interface OnGameObjectCreatedListener extends EventListener {
        void onCreated(GameObject gameObject);
    }

    public interface OnGameObjectDestroyedListener extends EventListener {
        void onDestroyed(GameObject gameObject);
    }

    private static final class IdGenerator {
        private static int id = Integer.MIN_VALUE;

        public static int getNextId() {
            return id++;
        }
    }
}