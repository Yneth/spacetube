package ua.abond.engine.gameobject.behaviours;

import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.Component;

public abstract class Behaviour implements Component {
    public final GameObject gameObject;
    public boolean activated = true;

    public Behaviour(GameObject gameObject) {
        this.gameObject = gameObject;
    }
}
