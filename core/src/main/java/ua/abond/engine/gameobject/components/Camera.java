package ua.abond.engine.gameobject.components;


import com.badlogic.gdx.math.collision.BoundingBox;

public interface Camera extends Component {
    void setPosition(float x, float y, float z);
    void setLookAt(float x, float y, float z);

    boolean isMainCamera();

    boolean isInFrustum(BoundingBox box);

    void resize(int width, int height);

    void setFieldOfView(int fieldOfView);
}
