package ua.abond.engine.gameobject.components;

import ua.abond.spacetube.engine.renderers.RenderableVisitor;

public interface Renderable extends Component {
    void render(RenderableVisitor visitor);
    boolean isVisible(final Camera camera, float width, float height);
}
