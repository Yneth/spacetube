package ua.abond.engine.gameobject.behaviours;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import ua.abond.engine.gameobject.GameObject;

// TODO: change to component
public class Transform extends Behaviour {
    public Quaternion angle = new Quaternion();
    public Vector3 position = new Vector3(0, 0, 0);
    public Vector3 scale = new Vector3(1.0f, 1.0f, 1.0f);

    public Transform(GameObject gameObject) {
        super(gameObject);
    }

    @Override
    public void update(float deltaTime) {

    }
}
