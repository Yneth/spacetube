package ua.abond.spacetube.engine.renderers;

import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.gameobject.components.Sprite;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxSprite;

public interface RenderableVisitor<SpriteT extends Sprite, RenderableT extends Renderable> {
    void visitSprite(SpriteT libGdxSprite);
    void visitModelInstance(RenderableT libGdxRenderable);
}
