package ua.abond.spacetube.engine.physics;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.utils.Disposable;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.components.Component;
import ua.abond.engine.gameobject.components.RigidBody;
import ua.abond.engine.gameobject.components.ComponentAdapter;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;

public class BulletRigidBody
        implements Component, ComponentAdapter<btRigidBody>, Disposable, RigidBody {
    public final GameObject parent;
    public final LibGdxRenderable libGdxRenderable;

    private final btRigidBody rigidBody;

    public BulletRigidBody(btRigidBody.btRigidBodyConstructionInfo constructionInfo, GameObject gameObject) {
        rigidBody = new btRigidBody(constructionInfo);

        parent = gameObject;
        libGdxRenderable = parent.getComponent(LibGdxRenderable.class);
    }

    @Override
    public void update(float deltaTime) {
        if (libGdxRenderable != null) {
            rigidBody.proceedToTransform(libGdxRenderable.getBody().transform);
        }
        else {
            rigidBody.proceedToTransform(new Matrix4().translate(parent.getComponent(Transform.class).position));
        }
    }

    @Override
    public btRigidBody getBody() {
        return rigidBody;
    }

    @Override
    public void dispose() {
        rigidBody.dispose();
    }
}

