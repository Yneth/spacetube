package ua.abond.spacetube.engine.gameobject.behaviours;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.collision.BoundingBox;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.gameobject.components.Component;
import ua.abond.engine.gameobject.components.ComponentAdapter;

public class LibGdxCamera implements Component,
        Camera,
        ComponentAdapter<com.badlogic.gdx.graphics.Camera> {
    private final GameObject gameObject;
    private final Transform transform;
    private final com.badlogic.gdx.graphics.Camera camera;

    public LibGdxCamera(GameObject gameObject, com.badlogic.gdx.graphics.Camera camera) {
        this.gameObject = gameObject;
        this.camera = camera;
        this.transform = gameObject.getComponent(Transform.class);
    }

    @Override
    public void setPosition(float x, float y, float z) {
        transform.position.set(x, y, z);
    }

    @Override
    public void setLookAt(float x, float y, float z) {
        camera.lookAt(x, y, z);
    }

    @Override
    public boolean isMainCamera() {
        return false;
    }

    @Override
    public boolean isInFrustum(BoundingBox box) {
        return camera.frustum.boundsInFrustum(box);
    }

    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
    }


    // TODO: create PerspectiveCamera interface to remove this method from Camera
    @Override
    public void setFieldOfView(int fieldOfView) {
        if (camera instanceof PerspectiveCamera) {
            ((PerspectiveCamera) camera).fieldOfView = fieldOfView;
        }
    }

    @Override
    public com.badlogic.gdx.graphics.Camera getBody() {
        return camera;
    }

    @Override
    public void update(float deltaTime) {
        camera.position.set(transform.position.x, transform.position.y, transform.position.z);
        camera.update();
//        camera.direction.set(transform.angle.getRoll(), transform.angle.getPitch(), transform.angle.getYaw());
    }

}
