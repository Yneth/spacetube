package ua.abond.spacetube.engine.scenes;

import com.badlogic.gdx.utils.IntMap;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.gameobject.components.RigidBody;
import ua.abond.engine.scenes.GameScene;

public class GameSceneImpl implements GameScene {
    private Camera mainCamera;

    private IntMap<GameObject> gameObjects = new IntMap<GameObject>();
    private IntMap<Renderable> renderables = new IntMap<Renderable>();
    private IntMap<RigidBody> rigidBodies = new IntMap<RigidBody>();

    @Override
    public void onStart() {
        if (mainCamera != null) return;

        for (GameObject gameObject : gameObjects.values()) {
            Camera cam = gameObject.getComponentByInterface(Camera.class);
            if (cam != null) {
                mainCamera = cam;
                break;
            }
        }

        if (mainCamera == null) {
            throw new MainCameraMissingException();
        }
    }

    @Override
    public GameObject instantiate(GameObject gameObject) {
        gameObjects.put(gameObject.getUniqueIdentifier(), gameObject);

        Renderable renderable = gameObject.getComponentByInterface(Renderable.class);
        if (renderable != null) {
            renderables.put(gameObject.getUniqueIdentifier(), renderable);
        }

        RigidBody rigidBody = gameObject.getComponentByInterface(RigidBody.class);
        if (rigidBody != null) {
            rigidBodies.put(gameObject.getUniqueIdentifier(), rigidBody);
        }

        return gameObject;
    }

    @Override
    public void remove(GameObject gameObject) {
        gameObjects.remove(gameObject.getUniqueIdentifier());
        int uid = gameObject.getUniqueIdentifier();
        if (renderables.containsKey(uid)) {
            renderables.remove(gameObject.getUniqueIdentifier());
        }
        if (rigidBodies.containsKey(uid)) {
            rigidBodies.remove(gameObject.getUniqueIdentifier());
        }
    }

    @Override
    public void update(float deltaTime) {
        for (GameObject gameObject : gameObjects.values()) {
            if (gameObject.active) {
                gameObject.update(deltaTime);
            }
        }
    }

    @Override
    public Iterable<GameObject> getGameObjects() {
        return gameObjects.values();
    }

    @Override
    public Iterable<Renderable> getRenderables() {
        return renderables.values();
    }

    @Override
    public Iterable<RigidBody> getRigidBodies() {
        return rigidBodies.values();
    }

    @Override
    public Camera getMainCamera() {
        return mainCamera;
    }

    @Override
    public void setMainCamera(Camera camera) {
        mainCamera = camera;
    }

    @Override
    public void dispose() {
        for (GameObject gameObject : gameObjects.values()) {
            GameObject.destroy(gameObject);
        }
        gameObjects.clear();
        renderables.clear();
        rigidBodies.clear();
    }
}
