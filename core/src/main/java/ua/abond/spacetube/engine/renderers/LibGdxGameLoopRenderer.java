package ua.abond.spacetube.engine.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.physics.GamePhysics;
import ua.abond.engine.renderers.GameRenderer;
import ua.abond.engine.scenes.GameScene;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxCamera;
import ua.abond.spacetube.scripts.Simulation;

public class LibGdxGameLoopRenderer implements GameRenderer {
    private final ModelBatch modelBatch;
    private final Environment environment;
    private final SpriteBatch spriteBatch;

    private Matrix4 viewMatrix;

    private int screenWidth;
    private int screenHeight;

    private RenderableVisitor renderableVisitor;

    public LibGdxGameLoopRenderer() {
        viewMatrix = new Matrix4();
        this.modelBatch = new ModelBatch();
        this.environment = new Environment();
        this.spriteBatch = new SpriteBatch();
        this.renderableVisitor = new RenderableGameLoopVisitor(modelBatch, spriteBatch, environment);

    }

    @Override
    public void render(float deltaTime, Iterable<GameObject> gameObjects) {

    }

    @Override
    public void render(float deltaTime, GameScene scene) {
        updateResolution();
        viewMatrix.setToOrtho2D(0, 0, screenWidth, screenHeight);
        LibGdxCamera camera = (LibGdxCamera) scene.getMainCamera();

        // TODO: add rendering layers
        // like opaque geom

        modelBatch.begin(camera.getBody());
        for (Renderable renderable : scene.getRenderables()) {
            renderable.render(renderableVisitor);
        }
        modelBatch.end();
        camera.update(deltaTime);
    }

    private void updateResolution() {
        this.screenHeight = Gdx.graphics.getHeight();
        this.screenWidth = Gdx.graphics.getWidth();
    }

    @Override
    public void renderDebug(float deltaTime, GamePhysics physics) {

    }

    @Override
    public void dispose() {
        viewMatrix = null;
        modelBatch.dispose();
        spriteBatch.dispose();
    }
}
