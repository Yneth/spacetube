package ua.abond.spacetube.engine;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import ua.abond.engine.GameController;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.physics.GamePhysics;
import ua.abond.engine.renderers.GameRenderer;
import ua.abond.engine.scenes.GameScene;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxCamera;
import ua.abond.spacetube.engine.physics.BulletPhysics;
import ua.abond.spacetube.engine.renderers.LibGdxGameLoopRenderer;
import ua.abond.spacetube.engine.scenes.GameSceneImpl;
import ua.abond.spacetube.scripts.BlockGenerator;
import ua.abond.spacetube.scripts.Simulation;
import ua.abond.spacetube.scripts.WorldBounds;
import ua.abond.spacetube.scripts.factories.SpaceShipFactory;

public class GameLoopController implements GameController {
    private final GamePhysics gamePhysics;
    private final GameRenderer gameRenderer;
    private final GameScene gameScene;

//    private IntMap<GameObject> gameObjects = new IntMap<GameObject>();

    public GameLoopController() {
        gamePhysics = new BulletPhysics(false);
        gameRenderer = new LibGdxGameLoopRenderer();

        // TODO: Remove game logic from loop controller
        GameObject.onCreated = new OnGameObjectCreatedEvent();
        GameObject.onDestroyedListener = new OnGameObjectDestroyedEvent();

        gameScene = new GameSceneImpl();

        gameScene.instantiate(
                SpaceShipFactory.
                        createShip(-90f, 0.5f, new Vector3(0.0f, Simulation.CENTER_Y - Simulation.RADIUS, 6.0f))
        );


        gameScene.instantiate(new WorldBounds(
                new Vector3(Simulation.CENTER_X, Simulation.CENTER_Y, 6.0f),
                new Vector3(10f, 6f, 6f)
        ));

        gameScene.instantiate(new GameObject() {
            {
                addComponent(new Transform(this));
                Camera cam = new LibGdxCamera(this, new PerspectiveCamera());
                cam.setLookAt(0.0f, 2.5f, 12.0f);
                cam.setPosition(0.0f, 2.5f, 0.0f);
                cam.setFieldOfView(67);
                addComponent(cam);
            }
        });

        gameScene.instantiate(new GameObject() {
            {
                addComponent(new BlockGenerator(this));
                GameObject.onCreated.onCreated(this);
            }
        });
        gameScene.onStart();
    }

    @Override
    public void update(float deltaTime) {
        gamePhysics.update(deltaTime);
//        for (GameObject gameObject : gameObjects.values()) {
//            if (gameObject.active) {
//                gameObject.update(deltaTime);
//            }
//        }
        gameScene.update(deltaTime);
    }

    @Override
    public void render(final float delta) {
//        gameRenderer.render(delta, gameObjects.values());
        gameRenderer.render(delta, gameScene);
        gameRenderer.renderDebug(delta, gamePhysics);
    }

    @Override
    public void dispose() {
//        for (GameObject gameObject : gameObjects.values()) {
//            GameObject.destroy(gameObject);
//        }
//        gameObjects.clear();
        gameScene.dispose();
        gamePhysics.dispose();
        gameRenderer.dispose();
    }

    private class OnGameObjectDestroyedEvent implements GameObject.OnGameObjectDestroyedListener {
        @Override
        public void onDestroyed(GameObject gameObject) {
//            if (gameObject == null) return;
//
//            BulletRigidBody bulletRigidBody = gameObject.getComponent(BulletRigidBody.class);
//            if (bulletRigidBody != null) {
//                gamePhysics.removeRigidBody(bulletRigidBody);
//            }
        }
    }

    private class OnGameObjectCreatedEvent implements GameObject.OnGameObjectCreatedListener {
        @Override
        public void onCreated(GameObject gameObject) {
//            BulletRigidBody bulletRigidBody = gameObject.getComponent(BulletRigidBody.class);
//            if (bulletRigidBody != null) {
//                gamePhysics.addRigidBody(bulletRigidBody);
//            }
//
//            gameObjects.put(gameObject.getUniqueIdentifier(), gameObject);
        }
    }
}
