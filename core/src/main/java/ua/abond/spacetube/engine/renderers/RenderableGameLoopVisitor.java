package ua.abond.spacetube.engine.renderers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxSprite;
import ua.abond.spacetube.engine.renderers.RenderableVisitor;

public class RenderableGameLoopVisitor implements RenderableVisitor<LibGdxSprite, LibGdxRenderable> {
    private final ModelBatch modelBatch;
    private final SpriteBatch spriteBatch;
    private final Environment environment;

    public RenderableGameLoopVisitor(ModelBatch modelBatch, SpriteBatch spriteBatch, Environment environment) {
        this.modelBatch = modelBatch;
        this.spriteBatch = spriteBatch;
        this.environment = environment;
    }

    @Override
    public void visitSprite(LibGdxSprite libGdxSprite) {
        spriteBatch.draw(libGdxSprite.getBody(), 0, 0);
    }

    @Override
    public void visitModelInstance(LibGdxRenderable libGdxRenderable) {
        modelBatch.render(libGdxRenderable.getBody(), environment);
    }
}
