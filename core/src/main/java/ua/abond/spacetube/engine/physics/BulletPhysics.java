package ua.abond.spacetube.engine.physics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.DebugDrawer;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.physics.bullet.dynamics.*;
import com.badlogic.gdx.physics.bullet.linearmath.btIDebugDraw;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.CollisionBehaviour;
import ua.abond.engine.physics.GamePhysics;

import java.util.HashMap;
import java.util.Map;

// TODO: This should be moved to its` own module
public class BulletPhysics implements GamePhysics<BulletRigidBody> {
    private static final int MAX_SUB_STEPS = 5;
    private static final float FIXED_TIME_STEP = 1.0f / 60.0f;

    private boolean isDebug;

    private BulletContactListener contactListener;
    private btDynamicsWorld dynamicsWorld;
    private btBroadphaseInterface broadphase;
    private btCollisionDispatcher collisionDispatcher;
    private btConstraintSolver constraintSolver;
    private btDefaultCollisionConfiguration collisionConfiguration;
    private DebugDrawer debugDrawer;

    private Map<Integer, BulletRigidBody> objects;

    public BulletPhysics(boolean isDebug) {
        Bullet.init();
        Gdx.app.log("BulletPhysics", "started");
        this.isDebug = isDebug;
        collisionConfiguration = new btDefaultCollisionConfiguration();
        collisionDispatcher = new btCollisionDispatcher(collisionConfiguration);
        broadphase = new btDbvtBroadphase();
        constraintSolver = new btSequentialImpulseConstraintSolver();

        debugDrawer = new DebugDrawer();
        debugDrawer.setDebugMode(
                debugDrawer.getDebugMode()
                        | btIDebugDraw.DebugDrawModes.DBG_DrawWireframe
        );

        dynamicsWorld = new btDiscreteDynamicsWorld(
                collisionDispatcher,
                broadphase,
                constraintSolver,
                collisionConfiguration
        );
        objects = new HashMap<Integer, BulletRigidBody>();

        contactListener = new BulletContactListener();

        dynamicsWorld.setDebugDrawer(debugDrawer);
    }

    @Override
    public void update(float delta) {
        dynamicsWorld.stepSimulation(delta, MAX_SUB_STEPS, FIXED_TIME_STEP);
    }

    @Override
    public void addRigidBody(BulletRigidBody rigidBody) {
        if (rigidBody == null) {
            return;
        }
        btRigidBody body = rigidBody.getBody();
        body.setUserValue(rigidBody.parent.getUniqueIdentifier());
        objects.put(body.getUserValue(), rigidBody);
        dynamicsWorld.addRigidBody(body);
    }

    @Override
    public void removeRigidBody(BulletRigidBody rigidBody) {
        if (rigidBody == null) return;
        btRigidBody body = rigidBody.getBody();
        objects.remove(body.getUserValue());
        dynamicsWorld.removeRigidBody(body);

        Gdx.app.log("Physics", "removing rigidbody");
    }

    @Override
    public void drawDebug(Camera camera) {
        if (!isDebug) {
            return;
        }
        debugDrawer.begin(camera);
        dynamicsWorld.debugDrawWorld();
        debugDrawer.end();
    }

    @Override
    public void dispose() {
        contactListener = null;
        objects.clear();
        contactListener.dispose();
        dynamicsWorld.dispose();
        debugDrawer.dispose();
        constraintSolver.dispose();
        broadphase.dispose();
        collisionDispatcher.dispose();
        collisionConfiguration.dispose();
    }

    private void onContactStartedCallback(int userValue0, int userValue1) {
        BulletRigidBody rigidBody0 = objects.get(userValue0);
        if (rigidBody0 == null) return;
        BulletRigidBody rigidBody1 = objects.get(userValue1);
        if (rigidBody1 == null) return;

        GameObject gameObject0 = rigidBody0.parent;
        GameObject gameObject1 = rigidBody1.parent;

        CollisionBehaviour collisionBehaviour0 = gameObject0.getComponent(CollisionBehaviour.class);

        if (collisionBehaviour0 != null) {
            collisionBehaviour0.onContactStarted(gameObject1);
        }
    }

    private void onContactProcessedCallback(int userValue0, int userValue1) {
        BulletRigidBody rigidBody0 = objects.get(userValue0);
        if (rigidBody0 == null) return;
        BulletRigidBody rigidBody1 = objects.get(userValue1);
        if (rigidBody1 == null) return;

        GameObject gameObject0 = rigidBody0.parent;
        GameObject gameObject1 = rigidBody1.parent;

        CollisionBehaviour collisionBehaviour0 = gameObject0.getComponent(CollisionBehaviour.class);

        if (collisionBehaviour0 != null) {
            collisionBehaviour0.onContactProcessed(gameObject1);
        }
    }

    private void onContactEndedCallback(int userValue0, int userValue1) {
        BulletRigidBody rigidBody0 = objects.get(userValue0);
        if (rigidBody0 == null) return;
        BulletRigidBody rigidBody1 = objects.get(userValue1);
        if (rigidBody1 == null) return;

        GameObject gameObject0 = rigidBody0.parent;
        GameObject gameObject1 = rigidBody1.parent;

        CollisionBehaviour collisionBehaviour0 = gameObject0.getComponent(CollisionBehaviour.class);

        if (collisionBehaviour0 != null) {
            collisionBehaviour0.onContactEnded(gameObject1);
        }
    }

    public class BulletContactListener extends ContactListener {
        @Override
        public void onContactStarted(int userValue0, boolean match0, int userValue1, boolean match1) {
            if (match0)
                onContactStartedCallback(userValue0, userValue1);
            if (match1)
                onContactStartedCallback(userValue1, userValue0);
        }

        @Override
        public void onContactEnded(int userValue0, boolean match0, int userValue1, boolean match1) {
            if (match0)
                onContactEndedCallback(userValue0, userValue1);
            if (match1)
                onContactEndedCallback(userValue1, userValue0);
        }

        @Override
        public void onContactProcessed(int userValue0, boolean match0, int userValue1, boolean match1) {
            if (match0)
                onContactProcessedCallback(userValue0, userValue1);
            if (match1)
                onContactProcessedCallback(userValue1, userValue0);
        }
    }
}
