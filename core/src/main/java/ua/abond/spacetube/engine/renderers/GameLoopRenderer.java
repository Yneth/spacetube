package ua.abond.spacetube.engine.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.renderers.GameRenderer;
import ua.abond.engine.scenes.GameScene;
import ua.abond.spacetube.assets.AssetLoader;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.physics.GamePhysics;
import ua.abond.spacetube.scripts.Simulation;

import java.util.Collection;

// TODO: Remove
public class GameLoopRenderer implements GameRenderer {
    private final RenderableVisitor renderableVisitor;

    private static final float FIELD_OF_VIEW = 67;
    private static final float MAX_TIME_VALUE = 1000.0f;

    private final boolean isShaderOn;

    private AssetLoader assetLoader;

    private float shaderTime = 0;

    private Environment lights;
    private PointLight shipLight;

    private PerspectiveCamera camera;

    private Texture backgroundBuffer;
    private Texture backgroundTexture;

    private int screenWidth;
    private int screenHeight;

    private FrameBuffer fbo;
    private ModelBatch modelBatch;
    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private ShaderProgram backgroundShader;

    private Matrix4 viewMatrix;

    public GameLoopRenderer() {
        shaderTime = 0;
        viewMatrix = new Matrix4();
        assetLoader = AssetLoader.getInstance();
        backgroundShader = assetLoader.BACKGROUND_SHADER;
        backgroundTexture = assetLoader.BACKGROUND_TEXTURE;

        updateScreenResolution();

        isShaderOn = Gdx.app.getPreferences("space_tube_prefs").getBoolean("isShaderOn");

        setBatches();
        setCameraAndLights();

        renderableVisitor = new RenderableGameLoopVisitor(modelBatch, spriteBatch, lights);

        fbo = new FrameBuffer(Pixmap.Format.RGBA8888, screenWidth, screenHeight, false);
    }

    @Override
    public void render(final float delta, final Iterable<GameObject> gameObjects) {
        updateScreenResolution();

        viewMatrix.setToOrtho2D(0, 0, screenWidth, screenHeight);
        backgroundBuffer = getBackgroundBuffer(delta);
        renderBackground();

        if (gameObjects != null) {
//            spriteBatch.begin();
            modelBatch.begin(camera);
            for (GameObject gameObject : gameObjects) {
                if (!gameObject.active) {
                    continue;
                }
                Renderable renderable = gameObject.getComponentByInterface(Renderable.class);
                if (renderable == null) {
                    continue;
                }
                renderable.render(renderableVisitor);
            }
            modelBatch.end();
//            spriteBatch.end();
        }
        setProjectionAndCamera();
    }

    @Override
    public void render(float deltaTime, GameScene scene) {
        updateScreenResolution();

        Camera cam = scene.getMainCamera();
        if (cam == null) return;


    }

    @Override
    public void renderDebug(final float delta, final GamePhysics physics) {
        physics.drawDebug(camera);
    }

    private void updateShaderTime(final float delta) {
        shaderTime += delta * 1.5;
        if (shaderTime > MAX_TIME_VALUE)
            shaderTime = 0;
    }

    @Override
    public void dispose() {
        fbo.dispose();
        modelBatch.dispose();
        spriteBatch.dispose();
        shapeRenderer.dispose();
        backgroundTexture.dispose();

        if (backgroundBuffer != null) {
            backgroundBuffer.dispose();
        }
    }

    private void setProjectionAndCamera() {
        camera.position.set(0.0f, 2.5f, 0.0f);
        camera.lookAt(0.0f, 2.5f, 12.0f);
        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    public void resize(final int width, final int height) {
        fbo.dispose();
        fbo = null;
        camera = new PerspectiveCamera(FIELD_OF_VIEW, width, height);
        if (width >= height) {
            fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
        } else {
            fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
        }
    }

    private void setCameraAndLights() {
        camera = new PerspectiveCamera(FIELD_OF_VIEW, screenWidth, screenHeight);
        lights = new Environment();

        lights.add(getPointLight(
                Color.WHITE,
                new Vector3(Simulation.CENTER_X, Simulation.CENTER_Y, 7f),
                3f
        ));
        shipLight = getPointLight(
                Color.WHITE,
                new Vector3(Simulation.CENTER_X, Simulation.CENTER_Y, 0f),
                4f
        );
        lights.add(shipLight);
    }

    private PointLight getPointLight(final Color color, final Vector3 position, final float intensity) {
        return new PointLight().set(color, position, intensity);
    }

    private void setBatches() {
        spriteBatch = new SpriteBatch();//40);
        modelBatch = new ModelBatch();
        shapeRenderer = new ShapeRenderer(100);
    }

    //region Trash
    public void render(final Simulation simulation, final float delta) {
        updateScreenResolution();

        viewMatrix.setToOrtho2D(0, 0, screenWidth, screenHeight); // get 2d projection
        if (isShaderOn) {
            backgroundBuffer = getBackgroundBuffer(delta);
            renderBackground();
        }

//        shipLight.position.set(simulation.getShip().getPosition());
        renderGameObjects(simulation);

//        Gdx.gl.glDisable(GL20.GL_CULL_FACE); // for testing purposes
//        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST); // adds some fps drops
        renderScore(simulation);
//        Gdx.app.log("", modelBatch.get);

        setProjectionAndCamera();
    }

    public void render(Collection<RenderableProvider> renderableProviders, final float delta) {
        updateScreenResolution();

        viewMatrix.setToOrtho2D(0, 0, screenWidth, screenHeight); // get 2d projection
        if (isShaderOn) {
            backgroundBuffer = getBackgroundBuffer(delta);
            renderBackground();
        }

//        shipLight.position.set(simulation.getShip().getPosition());
//        renderGameObjects(simulation);

//        Gdx.gl.glDisable(GL20.GL_CULL_FACE); // for testing purposes
//        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST); // adds some fps drops
//        renderScore(simulation);
//        Gdx.app.log("", modelBatch.get);

        modelBatch.begin(camera);
        modelBatch.render(renderableProviders, lights);
        modelBatch.end();

        setProjectionAndCamera();
    }

    private void updateScreenResolution() {
        screenHeight = Gdx.graphics.getHeight();
        screenWidth = Gdx.graphics.getWidth();
    }

    private Texture getBackgroundBuffer(final float delta) {
        updateShaderTime(delta);

        backgroundShader.begin();
        backgroundShader.setUniformf("resolution", screenWidth, screenHeight);
        backgroundShader.setUniformf("time", shaderTime);
        backgroundShader.end();

        fbo.begin();
        Gdx.gl.glViewport(0, 0, screenWidth, screenHeight);

        spriteBatch.setShader(backgroundShader);
        spriteBatch.setProjectionMatrix(viewMatrix);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0f, 0f, screenWidth, screenHeight);
        spriteBatch.end();
        fbo.end();
        return fbo.getColorBufferTexture();
    }

    private void renderBackground() {
        spriteBatch.setShader(null);
        spriteBatch.begin();
        spriteBatch.disableBlending();
        spriteBatch.setColor(Color.WHITE);
        spriteBatch.draw(backgroundBuffer, 0, 0, screenWidth, screenHeight);
        spriteBatch.end();
    }

    private void renderScore(final Simulation simulation) {
        spriteBatch.begin();
        spriteBatch.enableBlending();
//        assetLoader.GAME_FONT.draw(spriteBatch, new StringBuilder("Score: ").append(simulation.getScore()).toString(), 0, 25);
        spriteBatch.end();
    }

    private void renderGameObjects(final Simulation simulation) {
        modelBatch.begin(camera);
//        modelBatch.render(simulation.getExplosions());
//        modelBatch.render(simulation.getShip(), lights);
//        modelBatch.render(simulation.getBlocks(), lights);
//        modelBatch.render(simulation.getBullets(), lights);
//        Bonus bonus = simulation.getBonus();
//        if (bonus != null) {
//            modelBatch.render(simulation.getBonus(), lights);
//        }
        modelBatch.end();
    }
    //endregion
}