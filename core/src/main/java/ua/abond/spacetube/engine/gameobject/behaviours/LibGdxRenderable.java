package ua.abond.spacetube.engine.gameobject.behaviours;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Disposable;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.gameobject.components.Component;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.gameobject.components.ComponentAdapter;
import ua.abond.spacetube.engine.renderers.RenderableVisitor;

public class LibGdxRenderable implements Component, Renderable, ComponentAdapter<ModelInstance>, Disposable {
    public final GameObject gameObject;
    public final BoundingBox boundingBox;
    private final ModelInstance modelInstance;

    public LibGdxRenderable(GameObject gameObject, Model model) {
        this.modelInstance = new ModelInstance(model);
        this.gameObject = gameObject;
        this.boundingBox = new BoundingBox();
        this.modelInstance.calculateBoundingBox(boundingBox);
    }

    public LibGdxRenderable(GameObject gameObject, Model model, String... rootNodeIds) {
        this.modelInstance = new ModelInstance(model, rootNodeIds);
        this.gameObject = gameObject;
        this.boundingBox = new BoundingBox();
        this.modelInstance.calculateBoundingBox(boundingBox);
    }

    @Override
    public void update(float deltaTime) {
        Transform transform = gameObject.getComponent(Transform.class);

        modelInstance.transform.set(transform.angle);
        modelInstance.transform.setTranslation(transform.position);
        modelInstance.transform.scale(transform.scale.x, transform.scale.y, transform.scale.z);
    }

    @Override
    public void render(RenderableVisitor visitor) {
        visitor.visitModelInstance(this);
    }

    @Override
    public boolean isVisible(Camera camera, float width, float height) {
//        return camera.frustum.boundsInFrustum(boundingBox);
        return camera.isInFrustum(boundingBox);
    }

    @Override
    public void dispose() {
    }

    @Override
    public ModelInstance getBody() {
        return modelInstance;
    }
}
