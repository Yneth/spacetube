package ua.abond.spacetube.engine.gameobject.behaviours;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.components.Camera;
import ua.abond.engine.gameobject.components.Component;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.engine.gameobject.components.ComponentAdapter;
import ua.abond.spacetube.engine.renderers.RenderableVisitor;

public class LibGdxSprite
        implements Component, Renderable,  ComponentAdapter<Sprite>,
            ua.abond.engine.gameobject.components.Sprite {
    private final GameObject gameObject;
    private final Sprite sprite;

    public LibGdxSprite(GameObject gameObject, Texture texture) {
        this.gameObject = gameObject;
        this.sprite = new Sprite(texture);
    }

    @Override
    public void update(float deltaTime) {
        Transform transform = gameObject.getComponent(Transform.class);

        sprite.setScale(transform.scale.x, transform.scale.y);
        sprite.setRotation(transform.angle.getAngle());
        sprite.translate(transform.position.x, transform.position.y);
    }

    @Override
    public void render(RenderableVisitor visitor) {
        visitor.visitSprite(this);
    }

    // TODO: implement this with a rectangle
    @Override
    public boolean isVisible(final Camera camera, float width, float height) {
        float x = sprite.getX();
        float y = sprite.getY();
        return x >= 0 && x <= width && y >= 0 && y <= height;
    }

    @Override
    public Sprite getBody() {
        return sprite;
    }
}
