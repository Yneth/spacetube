package ua.abond.spacetube.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class AssetLoader implements Disposable {
    //region Constants
    public static final float BOX_SIZE = 0.7f;

    public static final float BULLET_SIZE = 0.2f;

    public static final float SHIP_WIDTH = 0.7f;
    public static final float SHIP_HEIGHT = 1.3f;
    public static final float SHIP_DEPTH = 0.7f;
    public static final int SHIP_DIVISIONS = 4;

    public static final String SHOT_WAV = "shot.wav";
    public static final String EXPLOSION_WAV = "explosion.wav";

    public static final String MUSIC_MAINTHEME_MP3 = "music/maintheme.mp3";

    public static final String KENVECTOR_FONT = "kenvector";
    public static final String BACKGROUND_PNG = "background512.png";

    public static final String MENU_SKIN_ATLAS = "skins/greySkin.pack";
    public static final String MENU_SKIN_JSON = "skins/menuSkin.json";

    private static final AssetLoader INSTANCE = new AssetLoader();
    public static final String TUNNEL_EFFECT_FRAGMENT_GLSL = "TunnelEffect.fragment.glsl";
    public static final String TUNNEL_EFFECT_VERTEX_GLSL = "TunnelEffect.vertex.glsl";
    //endregion

    //region Asset variables
    private final AssetManager assetLoader = new AssetManager();

//    public final Model BULLET_MODEL;
//    public final Model BLOCK_MODEL;
//    public final Model SHIP_MODEL;
    public final Model EXPLOSION_MODEL;
    public final Model GAME_MODELS;

    public final ShaderProgram BACKGROUND_SHADER;
    public final Texture BACKGROUND_TEXTURE;

    public final Sound SHOT_SOUND;
    public final Sound EXPLOSION_SOUND;

    public final Music MAIN_THEME;

    public final Skin MENU_SKIN;

    public final BitmapFont GAME_FONT;
    //endregion

    private AssetLoader() {
        //region Load models
        ModelBuilder builder = new ModelBuilder();
        builder.begin();

        createModel(builder, "cone", "ship",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal, new Material(ColorAttribute.createDiffuse(Color.WHITE)))
                        .cone(SHIP_WIDTH, SHIP_HEIGHT, SHIP_DEPTH, SHIP_DIVISIONS);

        createModel(builder, "box", "block",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.RED))).box(BOX_SIZE, BOX_SIZE, BOX_SIZE);

        createModel(builder, "box", "lightBox",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.YELLOW))).box(BOX_SIZE, BOX_SIZE, BOX_SIZE);

        createModel(builder, "box", "mediumBox",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(new Color(1f, 0.5f, 0f, 1f)))).box(BOX_SIZE, BOX_SIZE, BOX_SIZE);

        createModel(builder, "box", "heavyBox",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.RED))).box(BOX_SIZE, BOX_SIZE, BOX_SIZE);

        createModel(builder, "cylinder", "bullet",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.PINK))).sphere(BULLET_SIZE, BULLET_SIZE, BULLET_SIZE, 4, 4);

        createModel(builder, "cylinder", "shield",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.BLUE))).sphere(BOX_SIZE, BOX_SIZE, BOX_SIZE, 4, 4);

        createModel(builder, "cylinder", "doubleShoot",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.GREEN))).sphere(BOX_SIZE, BOX_SIZE, BOX_SIZE, 4, 4);

        createModel(builder, "cylinder", "rayShoot",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal,
                new Material(ColorAttribute.createDiffuse(Color.RED))).sphere(BOX_SIZE, BOX_SIZE, BOX_SIZE, 4, 4);
        GAME_MODELS = builder.end();

        EXPLOSION_MODEL = loadExplosionModel(builder);
        //endregion

        //region Load sounds
        assetLoader.load(SHOT_WAV, Sound.class);
        assetLoader.load(EXPLOSION_WAV, Sound.class);
        assetLoader.load(MUSIC_MAINTHEME_MP3, Music.class);
        //endregion

        //region Load shader
        String fragmentShader = Gdx.files.internal(TUNNEL_EFFECT_FRAGMENT_GLSL).readString();
        String vertexShader = Gdx.files.internal(TUNNEL_EFFECT_VERTEX_GLSL).readString();
        BACKGROUND_SHADER = new ShaderProgram(vertexShader, fragmentShader);
        if (!BACKGROUND_SHADER.isCompiled())
            throw new GdxRuntimeException("Couldn't compile shader: " + BACKGROUND_SHADER.getLog());
        ShaderProgram.pedantic = false;
        //endregion

        //region Load skin
        assetLoader.load(MENU_SKIN_JSON, Skin.class, new SkinLoader.SkinParameter(MENU_SKIN_ATLAS));
        //endregion

        //region Load images
        assetLoader.load(BACKGROUND_PNG, Texture.class);
        //endregion

        assetLoader.finishLoading();

        //region Get references
        SHOT_SOUND = assetLoader.get(SHOT_WAV);
        EXPLOSION_SOUND = assetLoader.get(EXPLOSION_WAV);
        MAIN_THEME = assetLoader.get(MUSIC_MAINTHEME_MP3);

        MENU_SKIN = assetLoader.get(MENU_SKIN_JSON);

        GAME_FONT = MENU_SKIN.getFont(KENVECTOR_FONT);

        BACKGROUND_TEXTURE = assetLoader.get(BACKGROUND_PNG);
        //endregion
    }

    private MeshPartBuilder createModel(ModelBuilder builder, String id, String nodeId, int primitiveType,
                                        long attributes, Material material) {
        builder.node().id = nodeId;
        return builder.part(id, primitiveType, attributes, material);
    }

    public static AssetLoader getInstance() {
        return INSTANCE;
    }

    @Override
    public void dispose() {
        GAME_MODELS.dispose();
        EXPLOSION_MODEL.dispose();

        BACKGROUND_SHADER.dispose();

        //region Unload
        assetLoader.unload(SHOT_WAV);
        assetLoader.unload(EXPLOSION_WAV);
        assetLoader.unload(MUSIC_MAINTHEME_MP3);

        assetLoader.unload(MENU_SKIN_ATLAS);
        assetLoader.unload(MENU_SKIN_JSON);

        assetLoader.unload(BACKGROUND_PNG);
        //endregion
        assetLoader.dispose();
    }

    // TODO: delete
    private Model loadExplosionModel(ModelBuilder builder) {
        final Texture explosionTexture = new Texture(Gdx.files.internal("data/explode.png"), Pixmap.Format.RGBA4444, true);
        explosionTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.Linear);

        final Mesh explosionMesh = new Mesh(true, 4 * 16, 6 * 16, new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));


        //region Load vertices and indices
        float[] vertices = new float[4 * 16 * (3 + 2)];
        short[] indices = new short[6 * 16];
        int idx = 0;
        int index = 0;
        for (int row = 0; row < 4; row++) {
            for (int column = 0; column < 4; column++) {
                vertices[idx++] = 1;
                vertices[idx++] = 1;
                vertices[idx++] = 0;
                vertices[idx++] = 0.25f + column * 0.25f;
                vertices[idx++] = 0 + row * 0.25f;

                vertices[idx++] = -1;
                vertices[idx++] = 1;
                vertices[idx++] = 0;
                vertices[idx++] = 0 + column * 0.25f;
                vertices[idx++] = 0 + row * 0.25f;

                vertices[idx++] = -1;
                vertices[idx++] = -1;
                vertices[idx++] = 0;
                vertices[idx++] = 0f + column * 0.25f;
                vertices[idx++] = 0.25f + row * 0.25f;

                vertices[idx++] = 1;
                vertices[idx++] = -1;
                vertices[idx++] = 0;
                vertices[idx++] = 0.25f + column * 0.25f;
                vertices[idx++] = 0.25f + row * 0.25f;

                final int t = (4 * row + column) * 4;
                indices[index++] = (short) (t);
                indices[index++] = (short) (t + 1);
                indices[index++] = (short) (t + 2);
                indices[index++] = (short) (t);
                indices[index++] = (short) (t + 2);
                indices[index++] = (short) (t + 3);
            }
        }
        //endregion

        explosionMesh.setVertices(vertices);
        explosionMesh.setIndices(indices);

        builder.begin();
        builder.part("id", explosionMesh, GL20.GL_TRIANGLES, new Material(new BlendingAttribute(
                GL20.GL_SRC_COLOR, GL20.GL_ONE_MINUS_SRC_ALPHA), TextureAttribute.createDiffuse(explosionTexture)));
        return builder.end();
    }
}
