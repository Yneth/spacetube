package ua.abond.spacetube.views.impl;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ua.abond.spacetube.views.ScreenManager;
import ua.abond.spacetube.views.base.SpaceTubeMenuScreen;

public class GameOver extends SpaceTubeMenuScreen {
    private int currentHighScore = 0;

    private Label title;
    private Label highScoreLabel;
    private TextButton buttonRetry;
    private TextButton buttonMainMenu;

    public GameOver(int score) {
        super();
        title.setText(title.getText().append(score));
        if (prefs.contains("highScore")) {
            currentHighScore = prefs.getInteger("highScore");
            highScoreLabel.setText(highScoreLabel.getText().append(currentHighScore));
        }
        if (score > currentHighScore) {
            prefs.putInteger("highScore", score);
            prefs.flush();
        }
    }

    @Override
    protected void loadTable() {
        table.add(title).padBottom(40).row();
        table.add(highScoreLabel).padBottom(20).row();
        table.add(buttonRetry).size(150, 60).padBottom(20).row();
        table.add(buttonMainMenu).size(150, 60).padBottom(20).row();
    }

    @Override
    protected void loadStage() {
        buttonRetry = new TextButton("Retry", skin);
        buttonMainMenu = new TextButton("Main menu", skin);
        title = new Label("Your score: ", skin);
        highScoreLabel = new Label("Your high score: ", skin);
        //region Listeners
        buttonRetry.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().setScreen(new GameLoop());
            }
        });
        buttonMainMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().setScreen(new MainMenu());
            }
        });
        //endregion
    }
}
