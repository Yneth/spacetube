package ua.abond.spacetube.views.impl;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ua.abond.spacetube.views.ScreenManager;
import ua.abond.spacetube.views.base.SpaceTubeMenuScreen;

public class Settings extends SpaceTubeMenuScreen {
    private Label sensitivityTitle;
    private Label title;
    private TextButton buttonExit;
    private CheckBox musicCheckBox;
    private CheckBox soundCheckBox;
    private CheckBox shaderCheckBox;
    private Slider sensitivitySlider;

    private BooleanHolder isMusicOn;
    private BooleanHolder isSoundOn;
    private BooleanHolder isShaderOn;

    private float currentSensitivity;

    public Settings() {
        super();
        isMusicOn = new BooleanHolder(prefs.getBoolean("isMusicOn"));
        setupBinaryCheckBox(musicCheckBox, isMusicOn);

        isSoundOn = new BooleanHolder(prefs.getBoolean("isSoundOn"));
        setupBinaryCheckBox(soundCheckBox, isSoundOn);

        isShaderOn = new BooleanHolder(prefs.getBoolean("isShaderOn"));
        setupBinaryCheckBox(shaderCheckBox, isShaderOn);

        currentSensitivity = prefs.getFloat("sensitivity");
        sensitivitySlider.setValue(currentSensitivity);
    }

    private void setupBinaryCheckBox(CheckBox checkBox, BooleanHolder holder) {
        checkBox.setChecked(holder.getValue());
        addBinaryCheckBoxListener(checkBox, holder);
    }

    private void addBinaryCheckBoxListener(CheckBox checkBox, BooleanHolder holder) {
        checkBox.addListener(new BinaryClickListener(holder));
    }

    @Override
    protected void loadStage() {
        buttonExit = new TextButton("Return", skin);
        title = new Label("Settings", skin);

        musicCheckBox = new CheckBox(" play music", skin);

        soundCheckBox = new CheckBox(" play sound", skin);

        shaderCheckBox = new CheckBox(" show shader", skin);

        buttonExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                prefs.putFloat("sensitivity", sensitivitySlider.getMaxValue() - sensitivitySlider.getValue());
                prefs.putBoolean("isMusicOn", isMusicOn.getValue());
                prefs.putBoolean("isSoundOn", isSoundOn.getValue());
                prefs.putBoolean("isShaderOn", isShaderOn.getValue());
                prefs.flush();

                ScreenManager.getInstance().moveToPrevious();
            }
        });
        sensitivitySlider = new Slider(0f, 2f, 0.1f, false, skin);
        sensitivityTitle = new Label("Sensitivity:", skin);
    }

    @Override
    protected void loadTable() {
        table.add(title).padBottom(30).row();
        table.add(shaderCheckBox).padBottom(20).row();
        table.add(musicCheckBox).padBottom(20).row();
        table.add(soundCheckBox).padBottom(20).row();
        table.add(sensitivityTitle).padBottom(20).row();
        table.add(sensitivitySlider).size(200, 40).padBottom(20).row();
        table.add(buttonExit).size(120, 60).padBottom(20).row();
    }

    private static class BinaryClickListener extends ClickListener {
        private BooleanHolder value;

        public BinaryClickListener(BooleanHolder value) {
            this.value = value;
        }

        @Override
        public void clicked(InputEvent event, float x, float y) {
            value.setValue(!value.getValue());
        }
    }

    class BooleanHolder {
        private boolean value;

        public BooleanHolder(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return value;
        }

        public void setValue(boolean value) {
            this.value = value;
        }
    }
}


