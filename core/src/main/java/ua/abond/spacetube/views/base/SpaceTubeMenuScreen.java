package ua.abond.spacetube.views.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class SpaceTubeMenuScreen extends SpaceTubeScreen {
    protected Table table;
    protected Stage stage;
    protected final Skin skin;

    public SpaceTubeMenuScreen() {
        log("init");

        skin = assetLoader.MENU_SKIN;
        stage = new Stage();
        table = new Table();

        loadStage();
        loadTable();

        table.setFillParent(true);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    protected abstract void loadTable();

    protected abstract void loadStage();

    @Override
    public void draw(float delta) {
        stage.draw();
    }

    @Override
    public void update(float delta) {
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage = new Stage();
        table = new Table();

        loadTable();

        table.setFillParent(true);
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
    }
}
