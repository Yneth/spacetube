package ua.abond.spacetube.views;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import ua.abond.spacetube.views.base.SpaceTubeScreen;

import java.util.Stack;

public enum  ScreenManager {
    INSTANCE;

    private Game game;
    private final Stack<SpaceTubeScreen> screens;

    ScreenManager() {
        screens = new Stack<SpaceTubeScreen>();
    }

    public static ScreenManager getInstance() {
        return INSTANCE;
    }

    public void init(Game game) {
        this.game = game;
    }

    public void setScreen(SpaceTubeScreen screen) {
        setScreen(screen, false);
    }

    public void setScreen(SpaceTubeScreen screen, boolean cachePrevious) {
        Gdx.app.log(this.getClass().getSimpleName(), screens.toString());

        if (game.getScreen() != null) {
            game.getScreen().hide();
        }

        if (!cachePrevious && !screens.empty()) {
            clear();
        }

        game.setScreen(screen);

        screens.push(screen);
    }

    public void getNextScreen() {
        getNextScreen(false);
    }

    public void getNextScreen(boolean cache) {
        Gdx.app.log(this.getClass().getSimpleName(), screens.toString());

        int index = screens.search(game.getScreen());
        if (cache) {
            screens.peek().hide();
            game.setScreen(screens.get(index+1));
            return;
        }
        screens.remove(index).dispose();

        game.setScreen(screens.get(index));
    }

    public void moveToPrevious() {
        moveToPrevious(false);
    }

    public void moveToPrevious(boolean cache) {
        Gdx.app.log(this.getClass().getSimpleName(), screens.toString());

        SpaceTubeScreen previousScreen = null;
        if (!cache) {
            screens.pop().dispose();
            previousScreen = screens.peek();
        } else {
            screens.peek().hide();
            previousScreen = screens.elementAt(screens.size()-1);
        }
        game.setScreen(previousScreen);
    }

    private void clear() {
        for (SpaceTubeScreen screen : screens) {
            screen.dispose();
        }
        screens.clear();
    }

    public com.badlogic.gdx.Screen getCurrentScreen() {
        return game.getScreen();
    }

    public void dispose() {
        clear();
    }
}
