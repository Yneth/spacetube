package ua.abond.spacetube.views.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import ua.abond.spacetube.assets.AssetLoader;

public abstract class SpaceTubeScreen implements Screen {
    protected Preferences prefs;
    protected AssetLoader assetLoader;

    public SpaceTubeScreen() {
        init();
    }

    private void init() {
        assetLoader = AssetLoader.getInstance();
        prefs = Gdx.app.getPreferences("space_tube_prefs");
    }

    public abstract void update(float delta);

    public abstract void draw(float delta);

    public void render(float delta) {
        update(delta);
        draw(delta);
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log("SpaceTubeScreen", "resizing");
    }

    @Override
    public void show() {
        Gdx.app.log("SpaceTubeScreen", "show called");
    }

    @Override
    public void hide() {
        Gdx.app.log("SpaceTubeScreen", "hide called");
    }

    @Override
    public void pause() {
        Gdx.app.log("SpaceTubeScreen", "pause called");
    }

    @Override
    public void resume() {
        Gdx.app.log("SpaceTubeScreen", "resume called");
    }

    @Override
    public void dispose() {
        Gdx.app.log("SpaceTubeScreen", "disposed called");
    }

    protected void log(String message) {
        Gdx.app.log(SpaceTubeScreen.class.getSimpleName(), message);
    }
}
