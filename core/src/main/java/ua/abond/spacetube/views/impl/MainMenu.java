package ua.abond.spacetube.views.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ua.abond.spacetube.views.ScreenManager;
import ua.abond.spacetube.views.base.SpaceTubeMenuScreen;

public class MainMenu extends SpaceTubeMenuScreen {
    private Label title;
    private TextButton buttonPlay;
    private TextButton buttonExit;
    private TextButton buttonSettings;

    public MainMenu() {
        super();
    }

    @Override
    protected void loadStage() {
        buttonPlay = new TextButton("Play", skin);
        buttonExit = new TextButton("Exit", skin);
        buttonSettings = new TextButton("Settings", skin);
        title = new Label("Space Tube", skin);
        //region listeners
        buttonPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().setScreen(new GameLoop());
            }
        });
        buttonExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
        buttonSettings.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().setScreen(new Settings(), true);
            }
        });
        //endregion
    }

    @Override
    protected void loadTable() {
        table.add(title).padBottom(40).row();
        table.add(buttonPlay).size(150, 60).padBottom(20).row();
        table.add(buttonSettings).size(150, 60).padBottom(20).row();
        table.add(buttonExit).size(150, 60).padBottom(20).row();
    }
}
