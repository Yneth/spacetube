package ua.abond.spacetube.views.impl;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ua.abond.spacetube.views.ScreenManager;
import ua.abond.spacetube.views.base.SpaceTubeMenuScreen;

public class GamePause extends SpaceTubeMenuScreen {
    private Label title;
    private TextButton playButton;
    private TextButton exitButton;
    private TextButton settingsButton;

    public GamePause() {
        super();
    }


    @Override
    protected void loadTable() {
        table.add(title).padBottom(40).row();
        table.add(playButton).size(150, 60).padBottom(20).row();
        table.add(settingsButton).size(150, 60).padBottom(20).row();
        table.add(exitButton).size(150, 60).padBottom(20).row();
    }

    @Override
    protected void loadStage() {
        playButton = new TextButton("Continue", skin);
        settingsButton = new TextButton("Setting", skin);
        exitButton = new TextButton("Main menu", skin);
        title = new Label("Paused", skin);
        //region listeners
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().moveToPrevious();
            }
        });
        settingsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().setScreen(new Settings(), true);
            }
        });
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().setScreen(new MainMenu());
            }
        });
        //endregion
    }
}
