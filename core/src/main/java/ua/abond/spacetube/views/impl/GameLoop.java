package ua.abond.spacetube.views.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import ua.abond.engine.GameController;
import ua.abond.spacetube.engine.GameLoopController;
import ua.abond.spacetube.engine.renderers.GameLoopRenderer;
import ua.abond.spacetube.views.ScreenManager;
import ua.abond.spacetube.views.base.SpaceTubeScreen;

public class GameLoop extends SpaceTubeScreen {

    private final GameLoopRenderer renderer;
    private final GameController controller;

    private float sensitivity;
    private boolean isSoundOn;

    public GameLoop() {
        super();
        Gdx.app.log("GameLoop", "initialized");

        isSoundOn = prefs.getBoolean("isSoundOn");
        sensitivity = prefs.getFloat("sensitivity");

        renderer = new GameLoopRenderer();
        controller = new GameLoopController();
    }

    @Override
    public void dispose() {
        super.dispose();
        renderer.dispose();
        controller.dispose();
        Gdx.app.log("GameLoop", "Disposed game loop.");
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        renderer.resize(width, height);
    }

    @Override
    public void update(float delta) {
        controller.update(delta);

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            ScreenManager.getInstance().setScreen(new GamePause(), true);
        }
    }

    @Override
    public void draw(float delta) {
//        renderer.render(controller.getRenderables(), delta);
        controller.render(delta);
    }
}
