package ua.abond.spacetube.scripts.behaviors;

import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Behaviour;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;

public abstract class Weapon extends Behaviour {
    public float cooldown = 1.0f;

    private List<OnShotListener> onShotListeners = new ArrayList<OnShotListener>();

    private float timeAfterLastShoot = cooldown;

    public Weapon(GameObject gameObject) {
        super(gameObject);
    }

    protected abstract void doFire();

    public void fire() {
        if (timeAfterLastShoot < cooldown) {
            return;
        }
        onFire();
        doFire();
        timeAfterLastShoot = 0;
    }

    @Override
    public void update(float deltaTime) {
        timeAfterLastShoot += deltaTime;
    }

    public void attachOnShotListener(OnShotListener listener) {
        onShotListeners.add(listener);
    }

    private void onFire() {
        for (OnShotListener listener : onShotListeners) {
            listener.shot(new OnShotEvent(this));
        }
    }

    public interface OnShotListener extends EventListener {
        void shot(OnShotEvent onShotEvent);
    }

    public class OnShotEvent extends EventObject {
        private final Weapon gameObject;

        public OnShotEvent(Object source) {
            super(source);
            gameObject = (Weapon) source;
        }

        public Weapon getGameObject() {
            return gameObject;
        }
    }
}
