package ua.abond.spacetube.scripts;

import com.badlogic.gdx.math.Vector3;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.spacetube.scripts.behaviors.Weapon;
import ua.abond.spacetube.scripts.factories.BulletFactory;

public class Gun extends Weapon {
    public Gun(GameObject gameObject) {
        super(gameObject);
    }

    @Override
    protected void doFire() {
        Bullet bullet = BulletFactory.createBullet(
                new Vector3(gameObject.getComponent(Transform.class).position),
                Vector3.Z
        );
    }
}
