package ua.abond.spacetube.scripts.behaviors;

import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Behaviour;

import java.util.EventListener;
import java.util.EventObject;
import java.util.HashSet;
import java.util.Set;

public class Health extends Behaviour {
    public int health = 100;

    private Set<OnHurtListener> onHurtListeners = new HashSet<OnHurtListener>();
    private Set<OnDeathListener> onDeathListeners = new HashSet<OnDeathListener>();

    public Health(GameObject gameObject) {
        super(gameObject);
    }

    public void hit(int damage) {
        this.health -= damage;

        notifyOnHurtListeners();

        if (health <= 0) {
            notifyOnDeathListeners();
        }
    }

    public void attachOnDeathListener(OnDeathListener onDeathListener) {
        onDeathListeners.add(onDeathListener);
    }

    public void attachOnHurtListener(OnHurtListener onHurtListener) {
        onHurtListeners.add(onHurtListener);
    }

    private void notifyOnDeathListeners() {
        for (OnDeathListener listener : onDeathListeners) {
            listener.die(new OnDeathEvent(gameObject));
        }
    }

    private void notifyOnHurtListeners() {
        for (OnHurtListener listener : onHurtListeners) {
            listener.hurt(new OnHurtEvent(gameObject));
        }
    }

    @Override
    public void update(float deltaTime) {

    }

    public interface OnDeathListener extends EventListener {
        void die(OnDeathEvent onDeathEvent);
    }

    public class OnDeathEvent extends EventObject {
        private final GameObject gameObject;

        public OnDeathEvent(Object source) {
            super(source);
            this.gameObject = (GameObject) source;
        }

        public GameObject getGameObject() {
            return gameObject;
        }
    }

    public interface OnHurtListener extends EventListener {
        void hurt(OnHurtEvent onDeathEvent);
    }

    public class OnHurtEvent extends EventObject {
        private final GameObject gameObject;

        public OnHurtEvent(Object source) {
            super(source);
            this.gameObject = (GameObject) source;
        }

        public GameObject getGameObject() {
            return gameObject;
        }
    }
}
