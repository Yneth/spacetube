package ua.abond.spacetube.scripts;

import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.behaviors.Physics;
import ua.abond.spacetube.scripts.behaviors.Health;

public class Block extends GameObject {

    public Block() {
        addComponent(new Transform(this));
        addComponent(new Health(this));
        addComponent(new Physics(this));
        addComponent(new Gun(this));

        getComponent(Health.class).attachOnDeathListener(new Health.OnDeathListener() {
            @Override
            public void die(Health.OnDeathEvent onDeathEvent) {
                System.out.println("die");
            }
        });
        getComponent(Health.class).attachOnHurtListener(new Health.OnHurtListener() {
            @Override
            public void hurt(Health.OnHurtEvent onDeathEvent) {
                System.out.println("hurt");
            }
        });
    }
}
