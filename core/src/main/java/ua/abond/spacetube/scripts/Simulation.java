package ua.abond.spacetube.scripts;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import ua.abond.spacetube.assets.AssetLoader;

public class Simulation {
    //region Constants
    public static final float PLAYFIELD_MIN_Z = 0.0f;
    public static final float PLAYFIELD_MAX_Z = 40.0f;

    public static final float PLAYFIELD_MIN_Y = -1.0f;
    public static final float PLAYFIELD_MAX_Y = 6f;

    public static final float BOUNDIG_BOX_FACTOR = 0.5f;
    public static final float PLAYFIELD_HEIGHT = Math.abs(PLAYFIELD_MAX_Y) + Math.abs(PLAYFIELD_MIN_Y);
    public static final float RADIUS = (PLAYFIELD_HEIGHT / 2) - AssetLoader.SHIP_WIDTH - BOUNDIG_BOX_FACTOR;
    public static final float PLAYFIELD_MAX_X = RADIUS + AssetLoader.SHIP_WIDTH;
    public static final float PLAYFIELD_MIN_X = -PLAYFIELD_MAX_X;

    public static final float CENTER_X = (PLAYFIELD_MAX_X + PLAYFIELD_MIN_X) / 2;
    public static final float CENTER_Y = (PLAYFIELD_MAX_Y + PLAYFIELD_MIN_Y) / 2;

    public static final float MAX_BOX_SPEED = 1.5f;
    public static final float BULLET_LIFE_TIME = 0.5f;
    public static final float SPEED_INCREMENT = 0.0001f;

    public static final Vector3 BLOCK_DIRECTION = new Vector3(0.0f, 0.0f, -0.5f);
    public static final Vector3 BULLET_DIRECTION = new Vector3(0.0f, 0.0f, 1.0f);

    public static final float PI_DIV_180 = MathUtils.PI / 180f;
    //    private static final int MAX_BOX_COUNT = (int) (RADIUS * MathUtils.PI / (AssetLoader.BOX_SIZE));
    public static final int MAX_BOX_COUNT = (int) (2 * RADIUS * MathUtils.PI / (AssetLoader.BOX_SIZE + 0.5f));

    public static final float BONUS_DELAY_TIME = 5f;
    //endregion

//    //region Variables
//    private final SimulationListener listener;
//
//    private final Events eventDispatcher = new Events();
//
//    private AssetLoader assetLoader;
//
//    private int wave = 0;
//
//    private int score = 0;
//    private float blockVelocity = 0.1f;
//    private float timeAfterLastBonus = 0;
//    private float timeAfterLastShot = BULLET_LIFE_TIME;
//
//    private Bonus bonus;
//    private SpaceShip spaceShip;
////    private List<Block> blocks = new LinkedList<Block>();
//    private Array<Block> blocks = new Array<Block>();
//    private Array<Bullet> bullets = new Array<Bullet>();
//    private Array<Explosion> explosions = new Array<Explosion>();
//
//    private Array<Block> removedBlocks = new Array<Block>();
//    private Array<Bullet> removedBullets = new Array<Bullet>();
//    private Array<Explosion> removedExplosions = new Array<Explosion>();
//
//    private boolean isDone = false;
//    private int bulletShotCount = 1;
//    //endregion
//
//    //region API
//    public Simulation(final SimulationListener listener) {
//        this.listener = listener;
//        assetLoader = AssetLoader.getInstance();
//        loadLevel();
//        addObservers(BulletBlockCollisionEvent.class, new BulletCollisionListener());
//        addObservers(ShipBlockCollisionEvent.class, new ShipCollisionListener());
//        addObservers(BonusBlockCollisionEvent.class, new BonusBulletCollisionListener());
//    }
//
//    // Do not change order of update methods
//    public void update(float delta) {
//        updateDifficulty(); // should be before block generation
//        updateScore(delta);
//        updateWave(delta);
//        generateBonus(delta); // refactor to
//        generateBlocks(delta);// generate wave
//        updateShip(delta);
//        updateBonus(delta);
//        updateExplosions(delta);
//        updateShots(delta);
//        updateBlocks(delta);
//    }
//
//    private void updateBonus(float delta) {
//        if (bonus == null)
//            return;
//        bonus.move(delta, BLOCK_DIRECTION);
//        bonus.update(delta);
//
//        for (Bullet bullet : bullets) {
//            if (bonus.collide(bullet)) {
//                eventDispatcher.notify(new BonusBlockCollisionEvent(bonus, bullet));
//            }
//        }
//        if (!bonus.canMove())
//            bonus = null;
//    }
//
//    private void generateBonus(float delta) {
//        timeAfterLastBonus += delta;
//        if (timeAfterLastBonus < BONUS_DELAY_TIME || bonus != null || waveIsNotPassed())
//            return;
//        timeAfterLastBonus = 0;
//
//        int randomId = MathUtils.random(0, 2);
//        final float angleFactor = 360 / MAX_BOX_COUNT;
//        float randomAngle = MathUtils.random(1f, MAX_BOX_COUNT) * angleFactor;
//
//        Vector3 position = calculatePositionInCircle(randomAngle);
//        position.z = PLAYFIELD_MAX_Z - AssetLoader.BOX_SIZE / 2;
//        position.y += AssetLoader.BOX_SIZE / 2;
//
//        bonus = BonusFactory.getBonus(randomId, position, blockVelocity);
//    }
//
//    public void moveSpaceShipLeft(float delta) {
//        spaceShip.setAngle(spaceShip.getAngle() + 1);
////        spaceShip.setAngle(spaceShip.getAngle() + (delta * MathUtils.PI2/5.1f));
//        moveSpaceShip(delta);
//    }
//
//    public void moveSpaceShipRight(float delta) {
//        spaceShip.setAngle(spaceShip.getAngle() - 1);
////        spaceShip.setAngle(spaceShip.getAngle() - (delta * MathUtils.PI2/5.1f));
//        moveSpaceShip(delta);
//    }
//
//    public void shot(float delta) {
//        if (timeAfterLastShot < BULLET_LIFE_TIME)
//            return;
//        for (int i = 0; i < bulletShotCount; i++) {
//            listener.onShot();
//
//            timeAfterLastShot = 0; // reset time
//
//            bullets.add(generateBullet());
//        }
//    }
//
//    @Override
//    public void dispose() {
//    }
//    //endregion
//
//    //region Utility methods
//    private <L> void addObservers(Class<? extends GameEvent<L>> eventClass, L listener) {
//        eventDispatcher.listen(eventClass, listener);
//    }
//
//    private void loadLevel() {
//        spaceShip = new SpaceShip(assetLoader.GAME_MODELS,
//                "ship",
//                new Vector3(0.0f, CENTER_Y - RADIUS, 6.0f)
//        );
//        // rotate ship to the direction of the hole
//        spaceShip.transform.setToRotation(Vector3.Y, Vector3.Z);
//    }
//
//    private void updateWave(float delta) {
//        if (waveIsNotPassed()) {
//            return;
//        }
//        wave++;
//    }
//
//    private void updateDifficulty() {
//        if (waveIsNotPassed() && wave % 2 != 0) {
//            return;
//        }
//        if (blockVelocity < MAX_BOX_SPEED) {
//            blockVelocity += SPEED_INCREMENT;
//        }
//    }
//
//    private void updateScore(float delta) {
//        if (wave % 5 == 0) {
//            score += wave * delta;
//        }
//    }
//
//    private void generateBlocks(float delta) {
//        if (waveIsNotPassed()) {
//            return;
//        }
////        int blockCount = MathUtils.random(1, (MAX_BOX_COUNT + 1) / 4);
//        int blockCount = MathUtils.random(1, (int) ((MAX_BOX_COUNT + 1) / 1.5));//1.5));
//        final float angleFactor = 360 / blockCount;
//        for (int i = 0; i < blockCount; i++) {
//            blocks.add(buildBlock(blockCount, angleFactor));
//        }
//    }
//
//    private Block buildBlock(int blockCount, float angleFactor) {
//        float randomAngle = MathUtils.random(1f, blockCount + 1) * angleFactor;
//        Vector3 position = calculatePositionInCircle(randomAngle);
////        position.z = PLAYFIELD_MAX_Z - AssetLoader.BOX_SIZE;
//        position.z = PLAYFIELD_MAX_Z - AssetLoader.BOX_SIZE / 2;
//        position.y += AssetLoader.BOX_SIZE / 2;
//
//        Block result = null;
//        if (removedBlocks.size != 0) {
//            int lastIndex = removedBlocks.size - 1;
//            result = removedBlocks.get(lastIndex);
//            result.setPosition(position);
//            result.setVelocity(blockVelocity);
//            result.resetHealth();
//            removedBlocks.removeIndex(lastIndex);
//        } else {
//            result = BlockFactory.buildRandomBlock(position, blockVelocity);
//        }
//        return result;
//    }
//
//    private boolean waveIsNotPassed() {
////        if (blocks.isEmpty())
//        if (blocks.size == 0)
//            return false;
//        Block lastBlock = blocks.get(blocks.size- 1);
////        return lastBlock.getPosition().z > (PLAYFIELD_MAX_Z - 2 * AssetLoader.BOX_SIZE);
//        return lastBlock.getPosition().z > (PLAYFIELD_MAX_Z - (assetLoader.BOX_SIZE + 0.35) * 2);
//    }
//
//    private void updateShip(float delta) {
//        for (Block block : blocks) {
//            if (spaceShip.collide(block)) {
//                eventDispatcher.notify(new ShipBlockCollisionEvent(spaceShip, block));
//            }
//        }
//        spaceShip.update(delta);
//    }
//
//    private void updateShots(float delta) {
//        timeAfterLastShot += delta;
//        removedBullets.clear();
//        for (Bullet bullet : bullets) {
//            bullet.move(delta, BULLET_DIRECTION);
//
//            if (bullet.canMove()) {
//                for (Block block : blocks) {
//                    if (bullet.collide(block)) {
//                        eventDispatcher.notify(new BulletBlockCollisionEvent(bullet, block));
//                    }
//                }
//            }
//            if (!bullet.canMove()) removedBullets.add(bullet);
//            bullet.update(delta);
//        }
//        for (Bullet bullet : removedBullets) {
//            bullets.removeValue(bullet, false);
//        }
//    }
//
//    private void updateBlocks(float delta) {
//        for (Block block : blocks) {
//            block.move(delta, BLOCK_DIRECTION);
//            block.update(delta);
//            if (!block.canMove()) removedBlocks.add(block);
//        }
//        for (Block block : removedBlocks) {
//            blocks.removeValue(block, false);
//        }
//    }
//
//    private void moveSpaceShip(float delta) {
//        Vector3 newDirection = calculatePositionInCircle(spaceShip.getAngle());
//        newDirection.z = 0.0f;
//        spaceShip.setDirection(newDirection);
//        spaceShip.move(delta, newDirection);
//    }
//
//    private Vector3 calculatePositionInCircle(float angle) {
//        Vector3 direction = new Vector3();
//        float x = RADIUS * MathUtils.cos(angle * PI_DIV_180) + CENTER_X;
////        float x = RADIUS * MathUtils.cos(angle) + CENTER_X;
//        float y = RADIUS * MathUtils.sin(angle * PI_DIV_180) + CENTER_Y;
////        float y = RADIUS * MathUtils.sin(angle) + CENTER_Y;
//        direction.add(x, y, 0.0f);
//        return direction;
//    }
//
//    private void updateExplosions(float delta) {
//        removedExplosions.clear();
//        for (Explosion explosion : explosions) {
//            explosion.update(delta);
//            if (explosion.aliveTime > Explosion.EXPLOSION_LIVE_TIME) {
//                removedExplosions.add(explosion);
//            }
//        }
//
//        for (Explosion removedExplosion : removedExplosions) {
//            explosions.removeValue(removedExplosion, false);
//        }
//    }
//
//    private Bullet generateBullet() {
//        Bullet result = null;
//        result = new Bullet(
//                assetLoader.GAME_MODELS,
//                "bullet",
//                spaceShip.getPosition()
//        );
//        return result;
//    }
//
//
//    //region Getters and setters
//    public int getScore() {
//        return score;
//    }
//
//    public Array<Bullet> getBullets() {
//        return bullets;
//    }
//
//    public Array<Block> getBlocks() {
//        return blocks;
//    }
//
//    public Bonus getBonus() {
//        return bonus;
//    }
//
//    public Array<Explosion> getExplosions() {
//        return explosions;
//    }
//
//    public SpaceShip getShip() {
//        return spaceShip;
//    }
//
//    public boolean isGameOver() {
//        return isDone;
//    }
//    //endregion
//    //endregion
//
//    //region EventListeners
//    private class ShipCollisionListener implements ShipBlockCollisionListener {
//        @Override
//        public void onCollision(SpaceShip ship, Block block) {
//            ship.attack(block);
//            block.attack(ship);
//            if (!ship.isAlive()) {
//                explosions.add(new Explosion(assetLoader.EXPLOSION_MODEL, ship.getPosition()));
//                listener.onExplode();
//                ship.setCanMove(false);
//            }
//            if (block.isAlive()) {
//                explosions.add(new Explosion(assetLoader.EXPLOSION_MODEL, block.getTranslation()));
//                listener.onExplode();
//                block.setCanMove(false);
//                removedBlocks.add(block);
//            }
//            isDone = true;
//        }
//    }
//
//    private class BulletCollisionListener implements BulletBlockCollisionListener {
//        @Override
//        public void onCollision(Bullet bullet, Block block) {
//            bullet.attack(block);
//            block.attack(bullet);
//            if (!bullet.isAlive()) {
//                bullet.setCanMove(false);
//            }
//            if (!block.isAlive()) {
//                block.setCanMove(false);
//                removedBlocks.add(block);
//                score += block.getScore();
//                listener.onExplode();
//                explosions.add(new Explosion(assetLoader.EXPLOSION_MODEL, block.getTranslation()));
//            }
//        }
//    }
//
//    private class BonusBulletCollisionListener implements BonusCollisionListener {
//        @Override
//        public void onCollision(Bonus bonus, Bullet bullet) {
//            bonus.attack(bullet);
//            bullet.attack(bonus);
//            bonus.setCanMove(false);
//            bullet.setCanMove(false);
//            bonus.setActive(true);
//            switch (bonus.getType()) {
//                case DOUBLE_SHOOT:
//                    Gdx.app.log("", "double shoot");
//                    bulletShotCount = 2;
//                    break;
//                case RAY_SHOOT:
//                    Gdx.app.log("", "ray shoot");
//                    bullet.setHealth(100000);
//                    break;
//                case SHIELD:
//                    spaceShip.setHealth(250);
//                    Gdx.app.log("", "shield");
//                    break;
//                default:
//                    break;
//            }
//        }
//    }
//    //endregion
}
