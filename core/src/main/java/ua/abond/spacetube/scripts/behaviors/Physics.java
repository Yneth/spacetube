package ua.abond.spacetube.scripts.behaviors;

import com.badlogic.gdx.math.Vector3;
import ua.abond.engine.gameobject.behaviours.Behaviour;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.MovementType;
import ua.abond.engine.gameobject.GameObject;

public class Physics extends Behaviour {
    public Vector3 direction = new Vector3(1.0f, 1.0f, 0.0f);
    public Vector3 velocity = Vector3.Zero;
    public MovementType movementType = MovementType.INCREMENTAL;

    public Physics(GameObject gameObject) {
        super(gameObject);
    }

    @Override
    public void update(float deltaTime) {
        Transform transform = gameObject.getComponent(Transform.class);

        transform.position = movementType.move(transform.position, direction, velocity.x, deltaTime);
    }
}
