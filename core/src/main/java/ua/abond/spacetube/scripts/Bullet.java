package ua.abond.spacetube.scripts;

import ua.abond.engine.gameobject.GameObject;
import ua.abond.spacetube.scripts.behaviors.Physics;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.behaviors.Health;

public class Bullet extends GameObject {
    public Bullet() {
        addComponent(new Transform(this));
        addComponent(new Health(this));
        addComponent(new Physics(this));
    }
}
