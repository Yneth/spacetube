package ua.abond.spacetube.scripts;

import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.spacetube.scripts.behaviors.Health;
import ua.abond.spacetube.scripts.behaviors.ShipMoveBehaviour;
import ua.abond.spacetube.scripts.behaviors.ShipPhysics;

public class SpaceShip extends GameObject {

    public SpaceShip() {
        addComponent(new Transform(this));
        addComponent(new Health(this));
        addComponent(new ShipPhysics(this));
        addComponent(new Gun(this));
        addComponent(new ShipMoveBehaviour(this));
    }
}

