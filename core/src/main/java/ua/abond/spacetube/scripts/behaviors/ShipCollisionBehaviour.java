package ua.abond.spacetube.scripts.behaviors;

import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.components.CollisionBehaviour;

public class ShipCollisionBehaviour extends CollisionBehaviour {
    public ShipCollisionBehaviour(GameObject gameObject) {
        super(gameObject);
    }

    @Override
    public void onContactStarted(GameObject collidedWith) {

    }

    @Override
    public void onContactProcessed(GameObject collidedWith) {

    }

    @Override
    public void onContactEnded(GameObject collidedWith) {

    }
}
