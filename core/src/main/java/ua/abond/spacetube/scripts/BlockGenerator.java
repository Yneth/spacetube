package ua.abond.spacetube.scripts;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import ua.abond.spacetube.assets.AssetLoader;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Behaviour;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.behaviors.Physics;
import ua.abond.spacetube.scripts.behaviors.Health;
import ua.abond.spacetube.scripts.factories.BlockFactory;

import java.util.LinkedList;
import java.util.List;

public class BlockGenerator extends Behaviour {

    public int maxBlockPerWaveCount;

    private float velocity;
    private List<Block> blocks = new LinkedList<Block>();
    private List<Block> removedBlocks = new LinkedList<Block>();
    private float waveDelay = 4.0f;
    private float timeBeforeLastWave = waveDelay;

    public BlockGenerator(GameObject gameObject) {
        super(gameObject);

    }

    @Override
    public void update(float deltaTime) {
//        if (waveIsNotPassed()) {
//            return;
//        }
        timeBeforeLastWave += deltaTime;
        if (timeBeforeLastWave < waveDelay) {
            return;
        }
        timeBeforeLastWave = 0;
        int blockCount = MathUtils.random(1, (int) ((Simulation.MAX_BOX_COUNT + 1) / 1.5));
        final float anglePositions = 360 / blockCount;
        for (int i = 0; i < blockCount; i++) {
            Block generatedBlock = buildBlock(blockCount, anglePositions);
            blocks.add(generatedBlock);
        }
    }

    private boolean waveIsNotPassed() {
        if (blocks.size() == 0) {
            return false;
        }
        Vector3 lastBlockPosition = blocks.get(blocks.size() - 1).getComponent(Transform.class).position;
        // TODO: think about LibgdxView and BoundingBox
        return lastBlockPosition.z > (Simulation.PLAYFIELD_MAX_Z - (AssetLoader.getInstance().BOX_SIZE + 0.35) * 2);
    }

    private Block buildBlock(int blockCount, float anglePositions) {
        float randomAngle = MathUtils.random(1f, blockCount + 1) * anglePositions;
        Vector3 position = calculatePositionInCircle(randomAngle);
//        position.z = PLAYFIELD_MAX_Z - AssetLoader.BOX_SIZE;
        position.z = Simulation.PLAYFIELD_MAX_Z - AssetLoader.BOX_SIZE / 2;
        position.y += AssetLoader.BOX_SIZE / 2;

        Block result = null;
        if (removedBlocks.size() != 0) {
            int lastIndex = removedBlocks.size() - 1;
            result = removedBlocks.get(lastIndex);
            result.getComponent(Transform.class).position = position;
            result.getComponent(Physics.class).velocity = new Vector3(velocity, 0, 0);

            // TODO: remove hard coded health assign
            result.getComponent(Health.class).health = 100;

            removedBlocks.remove(lastIndex);
        } else {
            result = BlockFactory.createRandomBlock(position, new Vector3(0, 0, -1.0f));//BlockFactory.buildRandomBlock(position, blockVelocity);
        }
        return result;
    }

    private Vector3 calculatePositionInCircle(float angle) {
        Vector3 direction = new Vector3();
        float x = Simulation.RADIUS * MathUtils.cos(angle * Simulation.PI_DIV_180) + Simulation.CENTER_X;
//        float x = RADIUS * MathUtils.cos(angle) + CENTER_X;
        float y = Simulation.RADIUS * MathUtils.sin(angle * Simulation.PI_DIV_180) + Simulation.CENTER_Y;
//        float y = RADIUS * MathUtils.sin(angle) + CENTER_Y;
        direction.add(x, y, 0.0f);
        return direction;
    }
}
