package ua.abond.spacetube.scripts.behaviors;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import ua.abond.spacetube.scripts.Simulation;
import ua.abond.spacetube.scripts.MovementType;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.engine.gameobject.GameObject;

public class ShipPhysics extends Physics {
    public float angle = 0.0f;
    public float radius = 2.0f;
    private static final float PI_DIV_180 = MathUtils.PI / 180f;

    public ShipPhysics(GameObject gameObject) {
        super(gameObject);
        movementType = MovementType.DISCONTINUOUS;
    }

    @Override
    public void update(float deltaTime) {
        Transform transform = gameObject.getComponent(Transform.class);
        direction = calculatePositionInCircle(angle);

        transform.position = movementType.move(transform.position, direction, 1.1f, deltaTime);
    }

    private Vector3 calculatePositionInCircle(float angle) {
        Vector3 direction = new Vector3();
        float x = Simulation.RADIUS * MathUtils.cos(angle * PI_DIV_180) + Simulation.CENTER_X;
        float y = Simulation.RADIUS * MathUtils.sin(angle * PI_DIV_180) + Simulation.CENTER_Y;
        direction.add(x, y, 0.0f);
        return direction;
    }
}
