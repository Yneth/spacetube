package ua.abond.spacetube.scripts;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by Anton on 25.02.2015.
 */
public enum MovementType {
    //TODO: add delta time to this formula
    INCREMENTAL {
        @Override
        public Vector3 move(Vector3 position, Vector3 direction, final float velocity, final float delta) {
            Vector3 newPosition = position;
            newPosition.x += direction.x * velocity;// * delta * 32f;
            newPosition.y += direction.y * velocity;// * delta * 32f;
            newPosition.z += direction.z * velocity;// * delta * 32f;
            return newPosition;
        }
    },
    DISCONTINUOUS { // (eg saltatory) proceeding by leaps rather than by gradual transitions
        @Override
        public Vector3 move(Vector3 position, Vector3 direction, final float velocity, final float delta) {
            Vector3 newPosition = position;
//            if (direction.x != 0)
                newPosition.x = direction.x * velocity;
//            if (direction.y != 0)
                newPosition.y = direction.y * velocity;
            if (position.z != 0 && direction.z != 0)
                newPosition.z = direction.z * velocity;
            return newPosition;
        }
    },
    STATIC {
        @Override
        public Vector3 move(Vector3 position, Vector3 direction, float velocity, float delta) {
            return position;
        }
    };

    public abstract Vector3 move(Vector3 position, Vector3 direction, final float velocity, final float delta);
}
