package ua.abond.spacetube.scripts.factories;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import ua.abond.spacetube.engine.physics.BulletRigidBody;
import ua.abond.spacetube.assets.AssetLoader;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.behaviors.Weapon;
import ua.abond.engine.gameobject.components.CollisionBehaviour;
import ua.abond.spacetube.scripts.behaviors.Health;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;
import ua.abond.spacetube.scripts.CollisionFlags;
import ua.abond.spacetube.scripts.Gun;
import ua.abond.spacetube.scripts.SpaceShip;
import ua.abond.spacetube.scripts.behaviors.ShipPhysics;

public class SpaceShipFactory {
    private static final AssetLoader ASSET_LOADER = AssetLoader.getInstance();

    public static SpaceShip createShip(float angle, float radius, Vector3 position) {
        SpaceShip ship = new SpaceShip();
        ship.addComponent(new LibGdxRenderable(ship, ASSET_LOADER.GAME_MODELS, "ship"));

        ship.getComponent(Health.class).health = 100;

        ship.getComponent(ShipPhysics.class).angle = angle;
        ship.getComponent(ShipPhysics.class).radius = radius;

        ship.getComponent(Transform.class).position.set(position);
        ship.tag = "player";

        ship.getComponent(Transform.class).angle.set(new Quaternion(Vector3.X, 90f));

        ship.getComponent(Health.class).attachOnDeathListener(new Health.OnDeathListener() {
            @Override
            public void die(Health.OnDeathEvent onDeathEvent) {
                ASSET_LOADER.EXPLOSION_SOUND.play();
            }
        });

        ship.addComponent(new Gun(ship));

        ship.getComponent(Gun.class).attachOnShotListener(
                new Weapon.OnShotListener() {
                    @Override
                    public void shot(Weapon.OnShotEvent onShotEvent) {
                        ASSET_LOADER.SHOT_SOUND.play();
                    }
                }
        );
        ship.addComponent(new BulletRigidBody(new btRigidBody.btRigidBodyConstructionInfo(
                1.0f,
                new MotionState(ship.getComponent(LibGdxRenderable.class).getBody().transform),
//                new btBoxShape(new Vector3(ship.getComponent(LibGdxRenderable.class).transform.getScale(new Vector3())))
                new btBoxShape(new Vector3(0.5f, 0.5f, 0.5f))
        ), ship));

        BulletRigidBody bulletRigidBody = ship.getComponent(BulletRigidBody.class);
        bulletRigidBody.getBody().proceedToTransform(ship.getComponent(LibGdxRenderable.class).getBody().transform.translate(position));
        bulletRigidBody.getBody().setGravity(Vector3.Zero);
        bulletRigidBody.getBody().setContactCallbackFlag(CollisionFlags.SHIP_FLAG);
        bulletRigidBody.getBody().setContactCallbackFilter(CollisionFlags.OBJECT_FLAG);
//        bulletRigidBody.setCollisionFlags(bulletRigidBody.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);

        ship.addComponent(new CollisionBehaviour(ship) {
            @Override
            public void onContactStarted(GameObject collidedWith) {
                Gdx.app.log("Ship", "Contact started");
            }

            @Override
            public void onContactProcessed(GameObject collidedWith) {
                Gdx.app.log("Ship", "Contact processed");
            }

            @Override
            public void onContactEnded(GameObject collidedWith) {
                Gdx.app.log("Ship", "Contact ended");
            }
        });
        ship.onCreated.onCreated(ship);
        return ship;
    }
}
