package ua.abond.spacetube.scripts.factories;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import ua.abond.spacetube.assets.AssetLoader;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;
import ua.abond.spacetube.scripts.behaviors.Physics;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.engine.physics.BulletRigidBody;
import ua.abond.spacetube.scripts.Bullet;

public class BulletFactory {
    public static final AssetLoader ASSET_LOADER = AssetLoader.getInstance();

    public static Bullet createBullet(Vector3 position, Vector3 direction) {
        Bullet bullet = new Bullet();
        bullet.tag = "bullet";
        bullet.getComponent(Transform.class).position.set(position);
        bullet.getComponent(Physics.class).velocity.set(0.2f, 0, 0);

        bullet.getComponent(Physics.class).direction.set(direction);
        bullet.addComponent(new LibGdxRenderable(bullet, ASSET_LOADER.GAME_MODELS, "bullet"));

        BulletRigidBody bulletRigidBody = new BulletRigidBody(new btRigidBody.btRigidBodyConstructionInfo(
                1,
                new MotionState(bullet.getComponent(LibGdxRenderable.class).getBody().transform),
                new btBoxShape(new Vector3(bullet.getComponent(LibGdxRenderable.class).getBody().transform.getScale(new Vector3())))
        ), bullet);

        bulletRigidBody.getBody().setContactCallbackFlag(0);
        bulletRigidBody.getBody().setContactCallbackFilter(0);
        bulletRigidBody.getBody().proceedToTransform(bullet.getComponent(LibGdxRenderable.class).getBody().transform.translate(position));
        bulletRigidBody.getBody().setGravity(Vector3.Zero);
        bullet.addComponent(bulletRigidBody);

        bullet.onCreated.onCreated(bullet);
        return bullet;
    }
}
