package ua.abond.spacetube.scripts;

public class CollisionFlags {
    public static final short OBJECT_FLAG = 1 << 9;
    public static final short SHIP_FLAG = 2 << 9;
    public static final short WALL_FLAG = 9 << 9;
}
