package ua.abond.spacetube.scripts.behaviors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Behaviour;
import ua.abond.spacetube.scripts.Gun;

public class ShipMoveBehaviour extends Behaviour {
    public float sensitivity = 1.0f;

    public ShipMoveBehaviour(GameObject gameObject) {
        super(gameObject);
    }

    @Override
    public void update(float deltaTime) {
        ShipPhysics physics = gameObject.getComponent(ShipPhysics.class);


        if (Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer)) {
            float accelerometerY = Gdx.input.getAccelerometerY();
            if (accelerometerY < -sensitivity) {
                physics.angle++;
            }
            else if (accelerometerY > sensitivity) {
                physics.angle--;
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)) {
            physics.angle++;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) {
            physics.angle--;
        }

        if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            gameObject.getComponent(Gun.class).fire();
        }
    }
}
