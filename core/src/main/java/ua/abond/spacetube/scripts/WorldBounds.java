package ua.abond.spacetube.scripts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.engine.physics.BulletRigidBody;
import ua.abond.engine.gameobject.components.CollisionBehaviour;
import ua.abond.spacetube.scripts.factories.MotionState;

public class WorldBounds extends GameObject {
    public WorldBounds(Vector3 position, Vector3 bounds) {
        BulletRigidBody bulletRigidBody = new BulletRigidBody(new btRigidBody.btRigidBodyConstructionInfo(
                1, new MotionState(new Matrix4().translate(position)),
                new btBoxShape(bounds)
        ), this);

        bulletRigidBody.getBody().setContactCallbackFlag(CollisionFlags.WALL_FLAG);
        bulletRigidBody.getBody().setContactCallbackFilter(CollisionFlags.OBJECT_FLAG);

        bulletRigidBody.getBody().setCollisionFlags(bulletRigidBody.getBody().getCollisionFlags()
                | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK
        );

        addComponent(bulletRigidBody);
        Transform transform = new Transform(this);
        transform.position = position;
        addComponent(transform);
        CollisionBehaviour collisionBehaviour = new CollisionBehaviour(this) {
            @Override
            public void onContactStarted(GameObject collidedWith) {

            }

            @Override
            public void onContactProcessed(GameObject collidedWith) {

            }

            @Override
            public void onContactEnded(GameObject collidedWith) {
                Gdx.app.log("Bounds", "destroying");
                GameObject.destroy(collidedWith);
            }
        };
        addComponent(collisionBehaviour);
        onCreated.onCreated(this);
    }
}
