package ua.abond.spacetube.scripts.factories;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import ua.abond.spacetube.assets.AssetLoader;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;
import ua.abond.spacetube.engine.physics.BulletRigidBody;
import ua.abond.spacetube.scripts.behaviors.Physics;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.behaviors.Health;
import ua.abond.spacetube.scripts.Block;
import ua.abond.spacetube.scripts.CollisionFlags;

public class BlockFactory {
    private final static AssetLoader ASSET_LOADER = AssetLoader.getInstance();

    private BlockFactory() {
    }

    private enum BlockType {
        LIGHT, MIDDLE, HEAVY;
    }

    public static Block createRandomBlock(Vector3 position, Vector3 direction) {
        int randomIndex = MathUtils.random(0, BlockType.values().length - 1);

        return createBlock(BlockType.values()[randomIndex], position, direction);
    }

    public static Block createBlock(BlockType type, Vector3 position, Vector3 direction) {
        switch (type) {
            case LIGHT:
                return createLightBlock(position, direction);
            case MIDDLE:
                return createMiddleBlock(position, direction);
            case HEAVY:
                return createHeavyBlock(position, direction);
        }
        return null;
    }

    public static Block createLightBlock(Vector3 position, Vector3 direction) {
        Block block = createDefaultBlock(position, direction);
        block.name = "lightBox";
        block.getComponent(Health.class).health = 50;
        block.addComponent(new LibGdxRenderable(block, ASSET_LOADER.GAME_MODELS, "lightBox"));
        return setupMotionState(block);
    }

    public static Block createMiddleBlock(Vector3 position, Vector3 direction) {
        Block block = createDefaultBlock(position, direction);
        block.name = "middleBox";
        block.getComponent(Health.class).health = 100;
        block.addComponent(new LibGdxRenderable(block, ASSET_LOADER.GAME_MODELS, "mediumBox"));
        return setupMotionState(block);
    }

    public static Block createHeavyBlock(Vector3 position, Vector3 direction) {
        Block block = createDefaultBlock(position, direction);
        block.name = "heavyBox";
        block.getComponent(Health.class).health = 150;
        block.addComponent(new LibGdxRenderable(block, ASSET_LOADER.GAME_MODELS, "heavyBox"));

        return setupMotionState(block);
    }

    private static Block createDefaultBlock(Vector3 position, Vector3 direction) {
        Block block = new Block();
        block.tag = "enemy";

        block.getComponent(Health.class).attachOnHurtListener(new Health.OnHurtListener() {
            @Override
            public void hurt(Health.OnHurtEvent onDeathEvent) {
                ASSET_LOADER.EXPLOSION_SOUND.play();
            }
        });

        block.getComponent(Transform.class).position.set(position);
        block.getComponent(Physics.class).direction.set(direction);
        block.getComponent(Physics.class).velocity.set(0.2f, 0, 0);

        return block;
    }

    private static Block setupMotionState(Block block) {
        block.addComponent(new BulletRigidBody(new btRigidBody.btRigidBodyConstructionInfo(
                1.0f,
                new MotionState(block.getComponent(LibGdxRenderable.class).getBody().transform),
                new btBoxShape(new Vector3(block.getComponent(LibGdxRenderable.class).getBody().transform.getScale(new Vector3())))
        ), block));
        BulletRigidBody rigidBody3D = block.getComponent(BulletRigidBody.class);
        rigidBody3D.getBody().proceedToTransform(block.getComponent(LibGdxRenderable.class).getBody().transform.translate(
                        block.getComponent(Transform.class).position)
        );

        rigidBody3D.getBody().setContactCallbackFlag(CollisionFlags.OBJECT_FLAG);
        rigidBody3D.getBody().setContactCallbackFilter(CollisionFlags.SHIP_FLAG | CollisionFlags.WALL_FLAG);
//        rigidBody3D.setCollisionFlags(rigidBody3D.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
        rigidBody3D.getBody().setGravity(Vector3.Zero);
        block.onCreated.onCreated(block);
        return block;
    }
}