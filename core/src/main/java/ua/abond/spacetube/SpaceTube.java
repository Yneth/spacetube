package ua.abond.spacetube;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.physics.bullet.Bullet;
import ua.abond.spacetube.assets.AssetLoader;
import ua.abond.spacetube.views.ScreenManager;
import ua.abond.spacetube.views.base.SpaceTubeScreen;
import ua.abond.spacetube.views.impl.MainMenu;

public class SpaceTube extends Game {
    private Preferences prefs;

    private Music music;
    private FPSLogger fpsLogger;
    private AssetLoader assetLoader;

    @Override
    public void create() {
        Bullet.init(true);

        ScreenManager.getInstance().init(this);
        ScreenManager.getInstance().setScreen(new MainMenu());

        assetLoader = AssetLoader.getInstance();

        prefs = Gdx.app.getPreferences("space_tube_prefs");

        music = assetLoader.MAIN_THEME;
        music.setLooping(true);

        fpsLogger = new FPSLogger();
    }

    @Override
    public void dispose() {
        super.dispose();
        ScreenManager.getInstance().dispose();
        assetLoader.dispose();
    }

    @Override
    public void render() {
        clearScreen();
        super.render();

        if (prefs.getBoolean("isMusicOn")) {
            music.play();
        }
        else if (music.isPlaying()) {
            music.stop();
        }

        fpsLogger.log();
    }

    private void clearScreen() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public SpaceTubeScreen getScreen () {
        return (SpaceTubeScreen) super.getScreen();
    }
}
