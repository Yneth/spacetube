package ua.abond.spacetube.views;

import com.badlogic.gdx.backends.headless.HeadlessApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ua.abond.spacetube.GdxTestRunner;
import ua.abond.spacetube.SpaceTube;
import ua.abond.spacetube.views.impl.MainMenu;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 05.06.2015.
 */
@RunWith(GdxTestRunner.class)
public class ScreenManagerTest {
    @Mock
    private SpaceTube listener;

    private HeadlessApplication app;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        app = new HeadlessApplication(listener);
        ScreenManager.getInstance().init(listener);
    }

    @Test
    public void renderMethodIsInvokedAtLeastOnce() {
        verify(listener, atLeast(1)).render();
    }

    @Test
    public void createMethodIsInvokedAtLeastOnce() {
        verify(listener, atLeast(1)).create();
    }

    @Test
    public void pauseIsNotInvoked() {
        verify(listener, times(0)).pause();
        app.exit();

        waitFor(100);

        verify(listener, times(1)).pause();
    }

    @Test
    public void disposeIsNotInvoked() {
        verify(listener, times(0)).dispose();
        app.exit();

        waitFor(100);

        verify(listener, times(1)).dispose();
    }

    @Test
    public void testGetInstance() throws Exception {
        verify(listener, atLeast(1)).create();
        assertNotNull(ScreenManager.getInstance());
        assertNotNull(ScreenManager.getInstance().getCurrentScreen());
        assertTrue(ScreenManager.getInstance().getCurrentScreen() instanceof MainMenu);
    }

    private void waitFor(int millis) {
        try {
            Thread.sleep(millis);
        }
        catch (InterruptedException e) {
        }
    }

//    @Test
//    public void testChangeScreen() throws Exception {
//
//    }
//
//    @Test
//    public void testChangeScreen1() throws Exception {
//
//    }
//
//    @Test
//    public void testGetNextScreen() throws Exception {
//
//    }
//
//    @Test
//    public void testGetNextScreen1() throws Exception {
//
//    }
//
//    @Test
//    public void testMoveToPrevious() throws Exception {
//
//    }
//
//    @Test
//    public void testMoveToPrevious1() throws Exception {
//
//    }
//
//    @Test
//    public void testGetCurrentScreen() throws Exception {
//
//    }
}