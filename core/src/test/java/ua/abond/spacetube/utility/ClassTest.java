package ua.abond.spacetube.utility;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClassTest {
    @Test
    public void testClassHashCode() {
        assertEquals(ClassTest.class.hashCode(), getClass().hashCode());

    }
}
