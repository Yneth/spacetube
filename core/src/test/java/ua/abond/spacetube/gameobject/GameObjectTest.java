package ua.abond.spacetube.gameobject;

import com.badlogic.gdx.graphics.g3d.Model;
import org.junit.Before;
import org.junit.Test;
import ua.abond.engine.gameobject.GameObject;
import ua.abond.spacetube.engine.gameobject.behaviours.LibGdxRenderable;
import ua.abond.spacetube.scripts.behaviors.Physics;
import ua.abond.engine.gameobject.behaviours.Transform;
import ua.abond.spacetube.scripts.behaviors.Weapon;
import ua.abond.spacetube.scripts.behaviors.Health;
import ua.abond.engine.gameobject.components.Renderable;
import ua.abond.spacetube.scripts.Gun;

import static org.junit.Assert.*;

public class GameObjectTest {
    private GameObject gameObject;

    @Before
    public void init() {
        gameObject = new GameObject() {
            {
                tag = "testTag";
                name = "testName";
                addComponent(new Transform(this));
                addComponent(new Health(this));
                addComponent(new Physics(this));
                addComponent(new Gun(this));
                addComponent(new LibGdxRenderable(this, new Model(), ""));
            }
        };
    }

    @Test
    public void testInstantiate() {
        OnCreatedListenerTest listener = new OnCreatedListenerTest();

        GameObject.onCreated = listener;
        GameObject clone = GameObject.instantiate(gameObject);

        assertTrue(listener.isOnCreatedCalled());
        assertNotNull(clone);
        assertNotEquals(gameObject, clone);
    }

    @Test
    public void testDestroy() {
        OnDestroyedListenerTest listener = new OnDestroyedListenerTest();

        GameObject.onDestroyedListener = listener;
        GameObject.destroy(gameObject);

        assertNotNull(gameObject);

        assertNull(gameObject.getComponent(Transform.class));
        assertTrue(listener.isOnDestroyedCalled());
    }

    @Test
    public void testGetComponentByInterface() {
        assertNotNull(gameObject.getComponentByInterface(Renderable.class));
    }

    @Test
    public void testModelInstanceComponent() {
        LibGdxRenderable libGdxRenderable = gameObject.getComponent(LibGdxRenderable.class);
        assertNotNull(libGdxRenderable);
        assertNotNull(libGdxRenderable.transform);
    }

    @Test
    public void testGetComponent() {
        assertNotNull(gameObject.getComponent(Transform.class));
        assertNotNull(gameObject.getComponent(Health.class));
        assertNotNull(gameObject.getComponent(Physics.class));
    }

    @Test
    public void testCustomComponent() {
        gameObject.addComponent(new CustomHealth(gameObject));

        assertNotNull(gameObject.getComponent(CustomHealth.class));

        int hitsToDeath = 5;
        gameObject.getComponent(CustomHealth.class).hitsToDeath = hitsToDeath;

        assertNotNull(gameObject.getComponent(Health.class));

        assertEquals(hitsToDeath, gameObject.getComponent(CustomHealth.class).hitsToDeath);
    }

    @Test
    public void testGetComponentValues() {
        final int healthPoints = 1996;
//        gameObject.health.health = healthPoints;
        gameObject.getComponent(Health.class).health = healthPoints;
        assertEquals(healthPoints, gameObject.getComponent(Health.class).health);
    }

    @Test
    public void testGetWrongComponent() {
        gameObject = new GameObject();
        assertNull(gameObject.getComponent(Weapon.class));
    }

    @Test
    public void testGetComponents() {
        assertNotNull(gameObject.getComponents(Weapon.class));
    }

    @Test
    public void testGetWrongComponents() {
        gameObject = new GameObject();
        assertNull(gameObject.getComponents(Weapon.class));
        assertNull(gameObject.getComponents(Transform.class));
        assertNull(gameObject.getComponents(Health.class));
        assertNull(gameObject.getComponents(Physics.class));
    }

    @Test
    public void testOnHurtEvent() {
        final BooleanHolder isHurt = new BooleanHolder(false);

        Health health = gameObject.getComponent(Health.class);
        health.attachOnHurtListener(new Health.OnHurtListener() {
            @Override
            public void hurt(Health.OnHurtEvent onHurtEvent) {
                isHurt.setValue(true);
            }
        });

        health.hit(10);

        assertTrue(isHurt.getValue());
    }

    @Test
    public void testOnDeathEvent() {
        Health health = gameObject.getComponent(Health.class);

        health.health = 100;

        final BooleanHolder isDead = new BooleanHolder(false);

        health.attachOnDeathListener(new Health.OnDeathListener() {
            @Override
            public void die(Health.OnDeathEvent onDeathEvent) {
                isDead.setValue(true);
            }
        });

        health.hit(100);

        assertTrue(isDead.getValue());
    }

    private class OnDestroyedListenerTest implements GameObject.OnGameObjectDestroyedListener {
        private boolean onDestroyedCalled = false;

        @Override
        public void onDestroyed(GameObject gameObject) {
            onDestroyedCalled = true;
        }

        public boolean isOnDestroyedCalled() {
            return onDestroyedCalled;
        }
    }

    private class OnCreatedListenerTest implements GameObject.OnGameObjectCreatedListener {
        private boolean onCreatedCalled = false;

        @Override
        public void onCreated(GameObject gameObject) {
            onCreatedCalled = true;
        }

        public boolean isOnCreatedCalled() {
            return onCreatedCalled;
        }
    }

    private class CustomHealth extends Health {
        public int hitsToDeath;

        public CustomHealth(GameObject gameObject) {
            super(gameObject);
        }
    }

    private class BooleanHolder {
        private boolean value;

        public BooleanHolder(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return value;
        }

        public void setValue(boolean value) {
            this.value = value;
        }
    }
}